import datetime
import functools
import json

#import tango
import numpy as np
import taurus
from taurus.core import TaurusDevState
from taurus.core.tango import DevState
from taurus.external.qt import Qt, QtGui
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.display import TaurusLabel
from taurus.qt.qtgui.resource import getThemeIcon
import time
import logging

class EditMotorsDialog(QtGui.QDialog):
    """
    A Qt dialog with multiple line input. Used to edit the list of motors.
    """
    def __init__(self):
        super(EditMotorsDialog, self).__init__(None)
        self.setWindowTitle("Edit motor list")
        self.resize(398, 253)
        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.setLayout(self.verticalLayout)

        l1 = QtGui.QLabel("Edit list of scannable motors. One moveable per "
                          "line. Leave empty to use motors from sardana.")
        self.verticalLayout.addWidget(l1)

        self.motor_edit = QtGui.QPlainTextEdit(self)
        self.verticalLayout.addWidget(self.motor_edit)

        formlayout = QtGui.QFormLayout()
        self.layout().addLayout(formlayout)

        self.CancelButton = QtGui.QPushButton(self)
        self.CancelButton.setText("Cancel")
        self.CancelButton.clicked.connect(self.reject)
        formlayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.CancelButton)

        self.OkButton = QtGui.QPushButton(self)
        self.OkButton.setText("Ok")
        self.OkButton.clicked.connect(self.accept)
        formlayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.OkButton)

    @staticmethod
    def getMotors(current_motors):
        """
        Display a textedit with current motors in a dialog
        Return a list of new motors
        """
        dialog = EditMotorsDialog()
        for motor in current_motors:
            dialog.motor_edit.appendPlainText(motor)
        result = dialog.exec_()
        if result == QtGui.QDialog.Accepted:
            newmotorlist = []
            for motor in dialog.motor_edit.toPlainText().split('\n'):
                if len(motor) > 0:
                    newmotorlist.append(str(motor))
            return newmotorlist
        else:
            return None


class BasicExecutorWidget(TaurusWidget):
    """
    The basic executor is a widget with some simple inputs to start and stop ascan and a2scan.
    It is created to be a part of a taurusgui
    """
    doorNameChanged = Qt.pyqtSignal('QString')
    shortMessageEmitted = Qt.pyqtSignal('QString')

    def __init__(self, parent=None):
        TaurusWidget.__init__(self)
        self.initUI()
        self.setupQtConnections()
        self._doorName = ""
        self._macroserverName = ""
        self._scanStopTime = time.time()
        self.available_motors = []
        self.movable_configuration = None
        if parent is not None:
            parent.taurusMenu.addAction(getThemeIcon("preferences-system-session"),
                                            "Edit motor list...", self.editMotorList)
        self.registerConfigProperty("saveSettingsToStr", "readSettingsFromStr", "basicexecutor")
        self.log_signal = None

    def initUI(self):

        self.setMinimumSize(1, 30)

        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        self.setSizePolicy(sizePolicy)
        self.resize(10, 10)
        self.verticalLayout = QtGui.QVBoxLayout(self)

        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setSizeConstraint(QtGui.QLayout.SetMaximumSize)

        self.label = QtGui.QLabel("Moveable")
        self.gridLayout_2.addWidget(self.label, 2, 0, 1, 1)

        self.line_motor = QtGui.QLineEdit("dmot01")
        self.combo_motor = QtGui.QComboBox()
        self.combo_motor.setInsertPolicy(QtGui.QComboBox.InsertBeforeCurrent)
        self.combo_motor.setLineEdit(self.line_motor)
        self.line_motor.setReadOnly(True)
        self.gridLayout_2.addWidget(self.combo_motor, 2, 1, 1, 1)

        self.label_2 = QtGui.QLabel("Start Position")
        self.gridLayout_2.addWidget(self.label_2, 3, 0, 1, 1)
        self.line_start_pos = QtGui.QLineEdit("0")
        self.gridLayout_2.addWidget(self.line_start_pos, 3, 1, 1, 1)
        self.motor1_start_unit = TaurusLabel()
        self.gridLayout_2.addWidget(self.motor1_start_unit, 3, 2, 1, 1)

        self.label_3 = QtGui.QLabel("Final Position")
        self.gridLayout_2.addWidget(self.label_3, 4, 0, 1, 1)
        self.line_final_pos = QtGui.QLineEdit("100")
        self.gridLayout_2.addWidget(self.line_final_pos, 4, 1, 1, 1)
        self.motor1_final_unit = TaurusLabel()
        self.gridLayout_2.addWidget(self.motor1_final_unit, 4, 2, 1, 1)

        self.label_13 = QtGui.QLabel("2nd Moveable")
        self.gridLayout_2.addWidget(self.label_13, 5, 0, 1, 1)

        self.check_a2scan = QtGui.QCheckBox()
        self.gridLayout_2.addWidget(self.check_a2scan, 5, 1, 1, 1)
        self.label_motor2 = QtGui.QLabel("2nd Moveable")
        self.gridLayout_2.addWidget(self.label_motor2, 6, 0, 1, 1)

        self.line_motor2 = QtGui.QLineEdit("dummymotor02")
        self.combo_motor2 = QtGui.QComboBox()
        self.combo_motor2.setInsertPolicy(QtGui.QComboBox.InsertBeforeCurrent)
        self.combo_motor2.setLineEdit(self.line_motor2)
        self.gridLayout_2.addWidget(self.combo_motor2, 6, 1, 1, 1)

        self.label_start2 = QtGui.QLabel("2nd Start Pos")
        self.gridLayout_2.addWidget(self.label_start2, 8, 0, 1, 1)
        self.line_motor2_start = QtGui.QLineEdit("0")
        self.gridLayout_2.addWidget(self.line_motor2_start, 8, 1, 1, 1)
        self.motor2_start_unit = TaurusLabel()
        self.gridLayout_2.addWidget(self.motor2_start_unit, 8, 2, 1, 1)

        self.label_final2 = QtGui.QLabel("2nd Final Pos")
        self.gridLayout_2.addWidget(self.label_final2, 9, 0, 1, 1)
        self.line_motor2_final = QtGui.QLineEdit("100")
        self.gridLayout_2.addWidget(self.line_motor2_final, 9, 1, 1, 1)
        self.motor2_final_unit = TaurusLabel()
        self.gridLayout_2.addWidget(self.motor2_final_unit, 9, 2, 1, 1)

        self.label_4 = QtGui.QLabel("Nr intervals")
        self.gridLayout_2.addWidget(self.label_4, 10, 0, 1, 1)

        self.line_nr_interv = QtGui.QLineEdit("10")
        self.gridLayout_2.addWidget(self.line_nr_interv, 10, 1, 1, 1)

        self.label_5 = QtGui.QLabel("Integration time")
        self.gridLayout_2.addWidget(self.label_5, 11, 0, 1, 1)

        self.line_integ_time = QtGui.QLineEdit("0.1")
        self.gridLayout_2.addWidget(self.line_integ_time, 11, 1, 1, 1)

        self.horizontalLayout_8 = QtGui.QHBoxLayout()

        self.startButton = QtGui.QPushButton("Start")
        self.horizontalLayout_8.addWidget(self.startButton)

        self.stopButton = QtGui.QPushButton("Stop")
        self.stopButton.setEnabled(False)
        self.horizontalLayout_8.addWidget(self.stopButton)
        self.gridLayout_2.addLayout(self.horizontalLayout_8, 14, 1, 1, 1)

        self.label_6 = QtGui.QLabel("Step size")
        self.gridLayout_2.addWidget(self.label_6, 16, 0, 1, 1)

        self.line_step_size = QtGui.QLineEdit()
        self.gridLayout_2.addWidget(self.line_step_size, 16, 1, 1, 1)

        self.verticalLayout.addLayout(self.gridLayout_2)
        spacerItem = QtGui.QSpacerItem(20, 189, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.estimated_finish_label = QtGui.QLabel()
        self.verticalLayout.addWidget(self.estimated_finish_label)

        self.progressBar = QtGui.QProgressBar()
        self.progressBar.setEnabled(True)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(True)
        self.verticalLayout.addWidget(self.progressBar)

    def setupQtConnections(self):
        self.line_nr_interv.textEdited.connect(self.onNrIntervEdited)
        self.line_step_size.textEdited.connect(self.onStepSizeEdited)
        self.line_start_pos.textEdited.connect(functools.partial(self.onPosChanged, self.line_start_pos))
        self.line_final_pos.textEdited.connect(functools.partial(self.onPosChanged, self.line_final_pos))
        self.check_a2scan.stateChanged.connect(self.onCheckA2scan)
        self.stopButton.clicked.connect(self.onStopButtonClicked)
        self.startButton.clicked.connect(self.startButtonClicked)
        self.combo_motor.currentIndexChanged.connect(functools.partial(self.onMotorComboChanged,
                                                                       self.combo_motor,
                                                                       self.line_motor,
                                                                       self.motor1_start_unit,
                                                                       self.motor1_final_unit))
        self.combo_motor2.currentIndexChanged.connect(functools.partial(self.onMotorComboChanged,
                                                                        self.combo_motor2,
                                                                        self.line_motor2,
                                                                        self.motor2_start_unit,
                                                                        self.motor2_final_unit))
        self.onCheckA2scan()
        self.onNrIntervEdited()

    def set_log_signal(self, signal):
        self.log_signal = signal

    def editMotorList(self):
        """
        Display dialog to edit the list of available motors, then change motorlist
        """
        newmotors = EditMotorsDialog.getMotors(self.available_motors)
        if newmotors is not None:
            self.changeMotorList(newmotors)

    def changeMotorList(self, newmotorlist):
        """
        Change available motors and the comboboxes
        """
        if len(newmotorlist) < 1:
            sardanamotors = self.getSardanaMotors()
            for combo in [self.combo_motor, self.combo_motor2]:
                idx = combo.currentIndex()
                combo.clear()
                combo.addItems(sardanamotors)
                combo.setCurrentIndex(idx)
            return
        for combo in [self.combo_motor, self.combo_motor2]:
            idx = combo.currentIndex()
            combo.clear()
            combo.addItems(newmotorlist)
            combo.setCurrentIndex(idx)
        self.available_motors = newmotorlist

    def getSardanaMotors(self):
        """
        This will ask the macroserver for elements and then return
        a list of moveables
        """
        macroserver = taurus.Device(self._macroserverName)
        sardanamotors = []
        sardanamotors.extend(macroserver.getElementNamesOfType("motor"))
        sardanamotors.extend(macroserver.getElementNamesOfType("pseudomotor"))
        # Sort the list alphabetically
        sardanamotors = sorted(sardanamotors, key=str.lower)

        if self.movable_configuration is not None:
            # Filter the list to keep only configured motors.
            sardanamotors_configured_to_include = []
            for sardanamotor in sardanamotors:
                if self.movable_configuration.should_include(sardanamotor):
                    sardanamotors_configured_to_include.append(sardanamotor)
            logging.debug("getSardanaMotors: self.movable_configuration causes keeping %d out of %d Sardana motors" %
                (len(sardanamotors_configured_to_include),
                len(sardanamotors)))
            sardanamotors = sardanamotors_configured_to_include
        else:
            logging.debug("getSardanaMotors: self.movable_configuration is None so not filtering any Sardana motors")

        return sardanamotors

    @staticmethod
    def onMotorComboChanged(combo, motor_line, line_start_unit, line_final_unit):
        """
        Motor combo has changed. If changed to other... then make it editable.
        Update the unit label models.
        """
        motor = combo.currentText()

        unitmodel = str(motor)+"/position?configuration=unit"
        for line in [line_start_unit, line_final_unit]:
            line.setText("")
            line.setModel(unitmodel)
            line.setBgRole("None")

    def onCheckA2scan(self):
        """
        Checkbox clicked to choose a2scan, display and hide approporiate input widgets.
        """
        widgets = [self.label_motor2,
                   self.label_start2,
                   self.label_final2,
                   self.line_motor2_start,
                   self.line_motor2_final,
                   self.motor2_start_unit,
                   self.motor2_final_unit,
                   self.combo_motor2]
        if self.check_a2scan.isChecked():
            [w.show() for w in widgets]
        else:
            [w.hide() for w in widgets]

    def onPosChanged(self, lineedit):
        """
        Calculate the step size when start or finalposition is changed
        """
        pos = lineedit.text().replace(",", ".")
        lineedit.setText(pos)
        self.onNrIntervEdited()

    def onStepSizeEdited(self):
        """
        Calculate the nr of intervals when the step size is changed
        """
        step_size = self.line_step_size.text()
        step_size = step_size.replace(",", ".")
        self.line_step_size.setText(step_size)
        final_pos = self.line_final_pos.text()
        start_pos = self.line_start_pos.text()
        for text in [step_size, final_pos, start_pos]:
            if len(text) == 0:
                return
            try:
                float(text)
            except ValueError:
                return
        if float(step_size) == 0:
            return
        nrinterv = np.abs((float(final_pos)-float(start_pos))/float(step_size))
        self.line_nr_interv.setText(str(int(nrinterv)))

    def onNrIntervEdited(self):
        """
        Calculate the step size when the number of intervals are changed
        """
        nr_interv = self.line_nr_interv.text()
        start_pos = self.line_start_pos.text()
        final_pos = self.line_final_pos.text()
        for text in [nr_interv, start_pos, final_pos]:
            if len(text) == 0:
                return
            try:
                float(text)
            except ValueError:
                return
        self.line_nr_interv.setText(str(int(float(nr_interv))))
        step_size = np.abs((float(final_pos)-float(start_pos))/int(nr_interv))
        self.line_step_size.setText(str(round(step_size, 4)))

    def doorState(self):
        """
        Return the state of the door, get it from taurus
        """
        if self.doorName() == "":
            return
        state = taurus.Device(self.doorName()).state
        return state

    def onStopButtonClicked(self):
        """
        Try telling the door to stop the macro
        """
        success = self.stopScan()
        if success:
            self.onMacroFinish()  # Enable buttons
        else:
            self.onMacroFinish()  # Enable start button
            if self.log_signal is None:
                Qt.QMessageBox.warning(None, "Error while stopping macro",
                                   "It was not possible to stop macro, " +
                                   "because state of the door was different than RUNNING or STANDBY")
            else:
                self.log_signal.emit("WARN", "It was not possible to stop macro " +
                                    "because state of the door was different than RUNNING or STANDBY")


    def stopScan(self):
        """
        Send StopMacro command to the tango door
        """
        doorState = self.doorState()
        door = taurus.Device(self.doorName())
        #if doorState in (tango.DevState.RUNNING, tango.DevState.STANDBY, TaurusDevState.Ready):
        if doorState in (DevState.RUNNING, DevState.STANDBY, TaurusDevState.Ready):
            try:
                door.command_inout("StopMacro")
            except Exception as e:
                if self.log_signal is not None:
                    self.log_signal.emit("ERROR", str(e))
            return True
        else:
            return False

    def startButtonClicked(self):
        """
        Start button is clicked, create the macro xml and send it with self.startScan
        """
        moveable = self.line_motor.text()
        start_pos = self.line_start_pos.text()
        final_pos = self.line_final_pos.text()
        nr_interv = self.line_nr_interv.text()
        integ_time = self.line_integ_time.text()

        moveable2 = self.line_motor2.text()
        start_pos2 = self.line_motor2_start.text()
        final_pos2 = self.line_motor2_final.text()
        try:
            self.doorState()
        except:
            if self.log_signal is None:
                Qt.QMessageBox.critical(None, "Error while reading door",
                                    "Not possible to read door state, is the correct door selected?")
            else:
                self.log_signal.emit("ERROR", "Not possible to read door state, is the correct door selected?")

        if self.check_a2scan.isChecked():  # A2SCAN
            macroline = '<macro name="a2scan" id="'+str(self.macroId())+'">' \
                        '<param name="motor1" value="'+moveable+'"/>' \
                        '<param name="start_pos1" value="'+start_pos+'"/>' \
                        '<param name="final_pos1" value="'+final_pos+'"/>' \
                        '<param name="motor2" value="'+moveable2+'"/>' \
                        '<param name="start_pos2" value="'+start_pos2+'"/>' \
                        '<param name="final_pos2" value="'+final_pos2+'"/>' \
                        '<param name="nr_interv" value="'+nr_interv+'"/>' \
                        '<param name="integ_time" value="'+integ_time+'"/></macro>'
        else:  # ASCAN
            macroline = '<macro name="ascan" id="'+str(self.macroId())+'">' \
                        '<param name="motor" value="'+moveable+'"/>' \
                        '<param name="start_pos" value="'+start_pos+'"/>' \
                        '<param name="final_pos" value="'+final_pos+'"/>' \
                        '<param name="nr_interv" value="'+nr_interv+'"/>' \
                        '<param name="integ_time" value="'+integ_time+'"/></macro>'
        #self.emit(Qt.SIGNAL('shortMessageEmitted'), "Basic executor starting scan")
        self.shortMessageEmitted.emit("Basic executor starting scan")
        self.startScan(macroline)

    def startScan(self, macroline):
        """
        Sends macroline to the door with runMacro
        """
        doorState = self.doorState()
        door = taurus.Device(self.doorName())
        #if doorState == tango.DevState.ON or doorState == tango.DevState.ALARM or doorState == TaurusDevState.Ready:
        if doorState == DevState.ON or doorState == DevState.ALARM or doorState == TaurusDevState.Ready:
            door.runMacro(str(macroline))
        else:
            self.onMacroStart()  # Enable stop buttons
            if self.log_signal is None:
                Qt.QMessageBox.warning(None, "Error while starting macro",
                                   "It was not possible to start macro,"
                                   " because state of the door was different than ON/STANDBY")
            else:
                self.log_signal.emit("WARN", "It was not possible to start macro "+
                                   "because state of the door was different than ON/STANDBY")

    def doorName(self):
        """
        Return the current doorname
        """
        return self._doorName

    def macroId(self):
        """
        Specify a unique macro id to keep track of what is launched by this executor
        Todo: 999 is not really that unique right
        """
        return 999

    def onDoorChanged(self, doorName):
        """
        Door name is changed from main scangui, change it here in the basicexecutor
        """
        self._doorName = doorName

    def onDoorError(self, data):
        """
        Raise an exception when there is an error in the door
         this creates a small popup,
        Todo, it would probably be wise to move this to the main scangui
        """
        try:
            msg = data[0]
            print(data)
            #self.emit(Qt.SIGNAL('shortMessageEmitted'), msg)
            self.shortMessageEmitted.emit(msg)
        except:
            pass
        if (time.time()-self._scanStopTime) < 1:
            # My scan stopped less than a second ago, so this error is mine
            if self.log_signal is None:
                Qt.QMessageBox.warning(None, data)
            else:
                self.log_signal.emit("WARN", str(data))


    def onMacroserverChanged(self, macroserverName):
        """
        Macroserver changed, change the motorlist
        this will update the motorlist if it is dynamic
        """
        self._macroserverName = macroserverName
        self.changeMotorList(self.available_motors)

    def onMacroStatusUpdated(self, data):
        """
        When the macro status is changed update the gui accordingly
        """
        try:
            macro = data[0]
        except (IndexError, TypeError):
            macro = None
        if macro is None:
            return
        try:
            data = data[1][0]
        except (IndexError, TypeError):
            return
        state, scanrange, step, scanid = data["state"], data["range"], data["step"], data["id"]
        if scanid is None:
            return
        try:
            scanid = int(scanid)
        except:
            return
        if scanid != self.macroId():
            # If not my scan then do nothing
            return
        if state == "start":
            self.onMacroStart()
        elif state == "step":
            self.onMacroStep(step)
        elif state == "finish":
            self.onMacroFinish()
        elif state == "stop":
            self.onMacroFinish()  # Enable controls
        else:
            #self.emit(Qt.SIGNAL('shortMessageEmitted'), "Basic executor: "+str(state)+"")
            self.shortMessageEmitted.emit("Basic executor: "+str(state)+"")
            self.onMacroFinish()  # Enable controls

    def onRecordDataUpdated(self, *args):
        """
        This is to extract the estimated time for ascan
        """
        try:
            data = args[0][1]['data']
            if 'estimatedtime' in data:
                if 'serialno' in data:
                    serialno = data['serialno']
                    serialnostr = "Scan nr: "+str(serialno)+"\n"
                else:
                    serialnostr = ""
                estimatedtime = data['estimatedtime']  # Seconds
                now = datetime.datetime.now()
                delta = datetime.timedelta(seconds=estimatedtime)
                finish = now+delta
                minutes, seconds = divmod(float(estimatedtime), 60)
                self.estimated_finish_label.setText(serialnostr+"Scan will take at least "+str(int(minutes)) +
                                                    " minutes " + str(int(seconds))+" seconds.\n" +
                                                    "Earliest finish: "+finish.strftime('%a %b %d %H:%M:%S %Y'))
            if 'starttime' in data:
                # Todo use starttime from data
                # starttime = data['starttime']
                self._starttime = datetime.datetime.now()
            if 'endtime' in data:
                endtime = data['endtime']
                now = datetime.datetime.now()
                delta = now - self._starttime
                minutes, seconds = divmod(delta.seconds, 60)
                self.estimated_finish_label.setText("Last scan took "+str(minutes)+" minutes "+str(seconds) +
                                                    " seconds.\n" + "Scan finished at: "+str(endtime))
        except Exception as er:
            print(er)

    def onMacroStart(self):
        """
        Macro is started, disable buttons in gui
        """
        self.stopButton.setEnabled(True)
        self.progressBar.setProperty("value", 0)
        self.line_motor.setEnabled(False)
        self.line_start_pos.setEnabled(False)
        self.line_final_pos.setEnabled(False)
        self.line_nr_interv.setEnabled(False)
        self.line_integ_time.setEnabled(False)
        self.startButton.setEnabled(False)
        self.line_step_size.setEnabled(False)
        self.check_a2scan.setEnabled(False)
        self.line_motor2_start.setEnabled(False)
        self.line_motor2_final.setEnabled(False)
        self.line_motor2.setEnabled(False)
        self.estimated_finish_label.setText("")

    def onMacroStep(self, step):
        """
        New macro step, update the progress bar
        """
        #self.emit(Qt.SIGNAL('shortMessageEmitted'), "Basic executor "+str(step)+"%")
        self.shortMessageEmitted.emit("Basic executor at {} %".format(int(step)))
        self.progressBar.setProperty("value", step)

    def onMacroFinish(self):
        """
        Macro is finished, enable buttons!
        """
        self._scanStopTime = time.time()
        self.check_a2scan.setEnabled(True)
        self.line_motor.setEnabled(True)
        self.line_start_pos.setEnabled(True)
        self.line_final_pos.setEnabled(True)
        self.line_nr_interv.setEnabled(True)
        self.line_integ_time.setEnabled(True)
        self.startButton.setEnabled(True)
        self.line_step_size.setEnabled(True)
        self.stopButton.setEnabled(False)
        self.line_motor2_start.setEnabled(True)
        self.line_motor2_final.setEnabled(True)
        self.line_motor2.setEnabled(True)

    def saveSettingsToStr(self):
        """
        Taurusgui saves this string when closing
        """
        x = {"line_motor": self.combo_motor.currentIndex(),
             "line_start_pos": str(self.line_start_pos.text()),
             "line_final_pos": str(self.line_final_pos.text()),
             "line_nr_interv": str(self.line_nr_interv.text()),
             "line_integ_time": str(self.line_integ_time.text()),
             "line_motor2": self.combo_motor2.currentIndex(),
             "line_motor2_start": str(self.line_motor2_start.text()),
             "line_motor2_final": str(self.line_motor2_final.text()),
             "a2scan": str(self.check_a2scan.isChecked()),
             "motors": self.available_motors}
        return json.dumps(x)

    def readSettingsFromStr(self, settingsstr):
        """
        Taurusgui gives us the string when starting
        """
        settings = json.loads(settingsstr)
        if "motors" in settings:
            newmotors = settings["motors"]
            self.changeMotorList(newmotors)
        if not "motors" in settings:
            newmotors = EditMotorsDialog.getMotors(["dummymotor01","dummymotor02"])
            self.changeMotorList(newmotors)
        if "line_motor" in settings:
            self.combo_motor.setCurrentIndex(settings["line_motor"])
        if "line_start_pos":
            self.line_start_pos.setText(settings["line_start_pos"])
        if "line_final_pos" in settings:
            self.line_final_pos.setText(settings["line_final_pos"])
        if "line_nr_interv" in settings:
            self.line_nr_interv.setText(settings["line_nr_interv"])
        if "line_integ_time" in settings:
            self.line_integ_time.setText(settings["line_integ_time"])
        if "line_motor2" in settings:
            self.combo_motor2.setCurrentIndex(settings["line_motor2"])
        if "line_motor2_start" in settings:
            self.line_motor2_start.setText(settings["line_motor2_start"])
        if "line_motor2_final" in settings:
            self.line_motor2_final.setText(settings["line_motor2_final"])
        if "a2scan" in settings:
            if settings["a2scan"] == "true" or settings["a2scan"] == "True":
                self.check_a2scan.setChecked(True)
        self.onNrIntervEdited()
