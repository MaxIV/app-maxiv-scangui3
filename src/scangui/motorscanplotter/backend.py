from taurus.external.qt import Qt, QtCore

from . import fileloader
from .scanitems import scanitem
from .scanitems.liveitem import LiveItem


class Backend(Qt.QObject):
    """
    The backend for the scangui keeps track of loaded and unloaded scans,
    The main reason for it is to have one place to send a qtsignal when the backend
    wants the gui to display a new scan.

    It also contains the models of the data.

    Signals:
    scanSelected: Tell the frontend that a scan has been selected, scan is self.selectedObject
    newScanAdded(scan): Tell the frontend that a new scan has been added to the backend.
    """
    scanSelected = QtCore.pyqtSignal()
    newScanAdded = QtCore.pyqtSignal(object)

    def __init__(self):
        QtCore.QObject.__init__(self)
        self.scanModel = scanitem.ScanModel()
        self.loadedScans = []
        self.specialItems = []
        self.selectedObject = None
        self.liveItem = None
        self.log_signal = None
        self.autoLoad = False

    def set_log_signal(self, signal):
        self.log_signal = signal
        if self.liveItem is not None:
            self.liveItem.set_log_signal(self.log_signal)

    def clear(self):
        """
        Empty the backend, used so qt connections doesn't have to be reset.
        Removing reference loops.
        """
        try:
            for item in self.loadedScans:
                item.cleanup()
            for item in self.specialItems:
                item.cleanup()
            for item in self.scanModel.fileList:
                item.cleanup()
        except Exception as er:
            print(er)

        self.scanModel.clear()
        self.loadedScans = []
        self.specialItems = []
        self.addLiveItem()

    def addLiveItem(self):
        """
        Add the liveitem of the online scan to the gui
        """
        self.liveItem = LiveItem()
        self.specialItems.append(self.liveItem)
        self.scanModel.appendRow(self.liveItem)
        self.newScanAdded.emit(self.liveItem)
        self.selectObject(self.liveItem)

    def addItem(self, item):
        """
        Add a new item to the model and select it in the gui
        """
        if isinstance(item, scanitem.ScanItem):
            self.loadedScans.append(item)
        else:
            self.specialItems.append(item)
        self.scanModel.appendRow(item)
        self.newScanAdded.emit(item)
        self.selectObject(item)

    def removeItem(self, item):
        """
        Remove a item from the list of scans
        """
        if isinstance(item, scanitem.FileItem):
            self.scanModel.fileList.remove(item)
            for scan in item.scanList:
                scan.cleanup()
                self.loadedScans.remove(scan)
        if item in self.specialItems:
            self.specialItems.remove(item)
        if item in self.loadedScans:
            self.loadedScans.remove(item)
        if hasattr(item, 'settingsWidget'):
            item.settingsWidget.deleteLater()  # ? Necessary
        item.cleanup()
        idx = self.scanModel.indexFromItem(item)
        self.scanModel.removeRow(idx.row())

    def setScanSettings(self, xidx, yidx, imgidx):
        """
        Set idx scan settings on all scans
        """
        for scan in self.loadedScans:
            scan.setXcolumnIndex(xidx)
            scan.setYcolumnIndex(yidx)
            scan.setImgcolumnIndex(imgidx)

    def getScan(self, index):
        """
        Returns the scan from the loadedscans with index, used when a newscan is loaded to get the settings of the last
        scan
        """
        if index >= len(self.loadedScans):
            return
        if index + len(self.loadedScans) < 0:
            return
        scan = self.loadedScans[index:]
        return scan[0]

    @staticmethod
    def scanlistfilterDisplayed(inputscans):
        """
        Filter inputscans and return which are displayed
        """
        addedScansList = []
        for scan in inputscans:
            if scan.isDisplayed:
                addedScansList.append(scan)
        return addedScansList

    @staticmethod
    def scanlistfilter2d(inputscans):
        """
        Filter the list of inputscans on which is currently a 2d scan
        """
        scanlist2d = []
        for scan in inputscans:
            if scan.is2d:
                scanlist2d.append(scan)
        return scanlist2d

    @staticmethod
    def scanlistfilter1d(inputscans):
        """
        Filter the list of scans on which is currently a 1d scan
        """
        scanlist1d = []
        for scan in inputscans:
            if not scan.is2d:
                scanlist1d.append(scan)
        return scanlist1d

    def loadFile(self, filename):
        """
        Load filename and add it to gui
        """
        fileitem = self.scanModel.getFileItem(filename)
        for scan in fileloader.getNewScans(fileitem, self.autoLoad):
            self.loadedScans.append(scan)
            self.newScanAdded.emit(scan)

    def selectObject(self, scan):
        """
        Tell the frontend that this is should be the currently selected scan
        """
        self.selectedObject = scan
        self.scanSelected.emit()
