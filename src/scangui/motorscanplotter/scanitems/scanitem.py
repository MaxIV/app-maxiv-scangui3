import logging
import os
import random

import h5py
import numpy as np
import pyqtgraph as pg
from taurus.external.qt import Qt, QtCore, QtGui

from .widgets.settings import CoreSettingsWidget, ScanSettingsWidget, FileSettingsWidget
from ..dataset import loadDataset, loadTitle

logger = logging.getLogger(__name__)


class ScanData(object):
    """
    ScanData is the core data container for one scan, loaded from file or retrieved from door
    """
    def __init__(self, filename=None, datasetname=None):
        if filename is None:
            filename = "None.h5"
        if datasetname is None:
            datasetname = "None"

        self.isAllDataLoaded = False
        self.isComplete = False
        self._columns = []
        self.title = ""
        self.datasetname = datasetname
        self.filename = filename
        self.header = []
        self.xColumnIndex = 0
        self.yColumnIndex = 0
        self.imgColumnIndex = -1
        self.imgTranspose = False

        self.oneDscanidxs = []
        self.twoDscanidxs = []
        self.threeDscanidxs = []

    def __str__(self):
        return f"Scan {self.filename} - {self.datasetname}"

    def load(self):
        if self.isAllDataLoaded:
            return
        loadTitle(self)
        loadDataset(self)
        # isComplete is set to true when end_time is found in the file
        # Until then, new data might be written to the file (scan in progress)
        # If the scan didn't finish properly, it might not have an end_time
        self.isAllDataLoaded = self.isComplete

    def hastwoDdata(self):
        """
        Does this scan contain any 2d plots, e.g. spectrums
        """
        if len(self.twoDscanidxs) > 0:
            return True
        else:
            return False

    def hasthreeDdata(self):
        """
        Does this scan contain any 3d plots
        """
        if len(self.threeDscanidxs) > 0:
            return True
        else:
            return False

    @property
    def columns(self):
        return self._columns

    def setColumnsFromRecords(self, values):
        """
        Transposing data from records to columns
        """
        columns = []
        numcolumns = len(self.header)  # UGLY could maybe be changed to check in values instead
        for _ in range(0, numcolumns):
            columns.append([])

        for record in values:
            for i in range(0, numcolumns):
                columns[i].append(record[i])

        for i in range(0, numcolumns):
            columns[i] = np.array(columns[i])

        self._columns = columns
        self._detectColumnDimensions()

    def setColumns(self, columns, oned=None, twod=None, threed=None):
        """
        Sets the data of scans to columns
        """
        self._columns = columns
        if oned is None or twod is None or threed is None:
            self._detectColumnDimensions()
        else:
            self.oneDscanidxs = oned
            self.twoDscanidxs = twod
            self.threeDscanidxs = threed

    def _detectColumnDimensions(self):
        """
        Checking which are 1D and which are 2D
        """
        columns = self._columns
        self.oneDscanidxs = []
        self.twoDscanidxs = []
        self.threeDscanidxs = []
        for i in range(0, len(columns)):
            if columns[i].ndim == 1:
                self.oneDscanidxs.append(i)
            elif columns[i].ndim == 2:
                self.twoDscanidxs.append(i)
            elif type(columns[i]) == h5py.h5r.Reference:
                # Three dimensional, only reference
                self.threeDscanidxs.append(i)
            else:
                raise Exception("Wrong dimension: "+str(columns[i].ndim))

    def setXcolumnIndex(self, index):
        """
        Set the current x column to be plotted
        """
        if index in self.oneDscanidxs or index == -1:
            self.xColumnIndex = index

    def setYcolumnIndex(self, index):
        """
        Set the current y column to be plotted
        """
        if index in self.oneDscanidxs or index == -1:
            self.yColumnIndex = index

    def setImgcolumnIndex(self, index):
        """
        Set the current image column to be displayed
        """
        if index not in self.twoDscanidxs:
            self.imgColumnIndex = -1
            return
        self.imgColumnIndex = index

    def setImgTranspose(self, state):
        """
        Set to true if the image should be flipped.
        """
        self.imgTranspose = state

    def getColumn(self, index):
        """
        Returns the selected column, beware that this is not a copy of the column!
        """
        if index >= len(self.header):
            return
        col = np.asarray(self._columns[index])
        return col

    def setHeader(self, header):
        self.header = header


class CoreScanItem(QtGui.QStandardItem):
    """
    An item that can be added in the treeview, with plots that can be added and removed
    """
    def __init__(self, text):
        QtGui.QStandardItem.__init__(self, text)
        self.setText(text)
        self.setCheckable(True)
        self.setEditable(False)
        self.setCheckState(QtCore.Qt.Unchecked)
        self.colorColumn = ColorColumn(self)
        self._plotItems = []
        self._isdisplayed = False
        self.settingsWidget = CoreSettingsWidget(self, "No Settings")

    def cleanup(self):
        """
        Cleanup to make sure that the garbage collector will clear properly.
        """
        self.settingsWidget.scan = None  # Reference loops
        self.settingsWidget = None
        self.colorColumn.scan = None

    def activate(self):
        """
        Item is clicked in main gui, propagate to widget
        """
        self.settingsWidget.activate()

    @property
    def isDisplayed(self):
        """
        Return True if this scanitem is currently plotted right now.
        """
        return self._isdisplayed

    def setIsDisplayed(self, status):
        """
        Change this when adding the plot in the gui.
        """
        self._isdisplayed = status

    @property
    def plotItems(self):
        return self._plotItems

    def setCheckState(self, state):
        QtGui.QStandardItem.setCheckState(self, state)
        if self.parent():
            self.parent().fixCheckState()

    def addPlotItem(self, item):
        """
        Add item to this plots list of pyqtgraph plotitems.
        """
        self._plotItems.append(item)

    def setColor(self, color):
        """
        Set the color of the plots. Sets the color of the colorcolumn in
         the treeview. Inherit to also set on plots,
        """
        self.colorColumn.setColor(color)

    def randomColor(self):
        """
        Sets the color to a random color
        """
        color = pg.intColor(random.randint(1, 10))
        self.setColor(color)

    def updatePlot(self):
        """
        Replot the plot.
         Do this when plotsettings changed or when the data has changed.
        """
        raise NotImplementedError

    def setTitle(self, newTitle):
        """
        Set the text of the scan that will become the tooltip when hovering
        """
        self.title = newTitle
        self.setToolTip(newTitle)

    @staticmethod
    def onPointClicked(plotItem, points):
        """
        When a scatterplot item is clicked this can display a textitem with
         x and y coordinate in the plot for three seconds.
        """
        point = points[0]
        x = point.pos().x()
        y = point.pos().y()
        tempText = pg.TextItem(text="X="+str(np.array(x, dtype=float))+", Y="+str(np.array(y, dtype=float)),
                               anchor=(0, 0))
        tempText.setPos(points[0].pos())
        vb = plotItem.getViewBox()
        vb.addItem(tempText)
        QtCore.QTimer.singleShot(3000, lambda: vb.removeItem(tempText))


class ColorColumn(QtGui.QStandardItem):
    """
    The column item that can be clicked to change color of the plot
    """
    def __init__(self, scan=None, color=None):
        QtGui.QStandardItem.__init__(self)
        self.color = color
        self.setEditable(False)
        self.setSelectable(False)
        self.scan = scan

    def setColor(self, color):
        brush = QtGui.QBrush(color)
        self.setBackground(brush)


class ScanItem(CoreScanItem, ScanData):
    """
    ScanItem is the qt top for ScanData
    ScanItem handles the plotting of Scandata and listing it in the treeview.
    """
    def __init__(self, filename=None, datasetname=None):
        CoreScanItem.__init__(self, datasetname)
        ScanData.__init__(self, filename, datasetname)
        self.useScatter = False
        self.plotDataItem = pg.PlotDataItem()
        self.imageItem = pg.ImageItem()

        self.addPlotItem(self.plotDataItem)
        self.addPlotItem(self.imageItem)

        self.imageData = None

        self.randomColor()

        self.settingsWidget = ScanSettingsWidget(self, datasetname)
        self.settingsWidget.settingsChanged.connect(self.updatePlot)

        self.yNormConst = 1
        self.is2d = False

        self.plotDataItem.sigPointsClicked.connect(self.onPointClicked)

        self.log_signal = None
        self.ignore_axis_selection_check = False

    def ignoreNextAxisSelectionCheck(self):
        # Will be reset to False when ignored once
        self.ignore_axis_selection_check = True

    def set_log_signal(self, signal):
        self.log_signal = signal

    def setColor(self, color):
        """
        Set the color of the curveplot or the scatterplot
        """
        CoreScanItem.setColor(self, color)
        pen = pg.mkPen(color)
        brush = pg.mkBrush(color)

        if self.useScatter:
            self.plotDataItem.setPen(None)
            self.plotDataItem.setSymbol('o')
            self.plotDataItem.setSymbolPen(pen)
            self.plotDataItem.setSymbolBrush(brush)
        else:
            self.plotDataItem.setSymbol(None)
            self.plotDataItem.setPen(pen)
            self.plotDataItem.setSymbolBrush(brush)

    def setYNorm(self, const):
        """
        Seth the constant that will divide the ydata in the plot
        """
        self.yNormConst = const

    def setUseScatter(self, yesno):
        """
        Set true to use scatterplot.
        Preservs colors when switching
        """
        self.useScatter = yesno
        if self.useScatter:
            pen = self.plotDataItem.opts['pen']
            self.plotDataItem.setPen(None)
            self.plotDataItem.setSymbol('o')
            self.plotDataItem.setSymbolPen(pen)
        else:
            self.plotDataItem.setSymbol(None)
            self.plotDataItem.setPen(self.plotDataItem.opts['symbolPen'])

    def setImgTranspose(self, state):
        """
        Set the image to be transposed
        """
        ScanData.setImgTranspose(self, state)
        self.updatePlot()

    def updatePlot(self):
        """
        Update the scanitems pyqtgraph items to correspond to newdata or new settings
        """
        self.load()
        if not self._columns:
            return
        xs = self.getColumn(self.xColumnIndex)
        ys = self.getColumn(self.yColumnIndex)

        try:
            test = xs/12.123
            test2 = ys/13.123
        except:
            print("There seems to be invalid numbers in xs or ys")
            return

        if self.imgColumnIndex < 0:
            if self.xColumnIndex == -1:
                xs = np.arange(0, len(ys))

            if self.yColumnIndex == -1:
                ys = np.arange(0, len(xs))
            self.show1d(xs, ys)
        else:
            img = self.getColumn(self.imgColumnIndex)
            img = np.flipud(img)
            if self.xColumnIndex == -1:
                xs = np.arange(0, img.shape[0])
            if self.yColumnIndex == -1:
                ys = np.arange(0, img.shape[1])
            self.show2d(xs, ys, img)

    def show1d(self, xs, ys):
        """
        Display a 1d plot with xs and ys
        """
        if self.yColumnIndex == -1 and self.xColumnIndex == -1:
            return

        if self.yNormConst:
            ys = np.copy(ys)/self.yNormConst

        if len(xs) != len(ys):
            if not self.ignore_axis_selection_check:
                filename = os.path.split(self.filename)[1]
                msg = "{}, {}: Both axis needs same length, x has len {} and y has len {}.".format(
                    filename,
                    self.datasetname,
                    len(xs),
                    len(ys)
                )
                if self.log_signal is None:
                    QtGui.QMessageBox.critical(None, "Error", msg)
                else:
                    self.log_signal.emit("ERROR", msg)
        else:
            self.plotDataItem.setData(xs, ys)
            self.imageItem.setFlag(self.imageItem.ItemHasNoContents)
            self.is2d = False

        # Reset ignore flag
        self.ignore_axis_selection_check = False

    def show2d(self, xs, ys, img):
        """
        Display 2d (image) plot img with xs and ys as scales
        """
        if self.imgTranspose:
            # flip is weird because of pyqtgraphs imageitem origin
            img = np.flipud(np.rot90(img, k=3))
        self.imageData = img  # This is important for roitool
        self.imageItem.setFlag(self.imageItem.ItemHasNoContents, False)
        self.plotDataItem.clear()
        self.imageItem.setImage(img)
        self.is2d = True

        if len(img) < 2:
            # No data so don't try to resize it
            return
        # Moving the image to fit the axis scales, NOTE this is only linear
        deltaxs = np.mean(np.diff(xs))  # Image should be one step wider than scatter plot
        deltays = np.mean(np.diff(ys))

        if not self.ignore_axis_selection_check:
            self.check2d_linearity(xs, ys)

        # Reset ignore flag
        self.ignore_axis_selection_check = False

        rect = QtCore.QRectF()
        # Set the coordinates of the rectangle's top-left corner to (x1, y1),
        # and the coordinates of its bottom-right corner to (x2, y2).
        rect.setCoords(xs[-1]+deltaxs/2, ys[0]-deltays/2, xs[0]-deltaxs/2, ys[-1]+deltays/2)
        self.imageItem.setRect(rect)

    @staticmethod
    def check_linearity(scale, limit):
        """
        Check if the scale has an relative offset (linearscale / realscale) in any point of more than limit
        """
        linearys = np.linspace(scale[0], scale[-1], num=len(scale))
        abserror = np.max(np.abs(scale - linearys))
        yrange = np.abs(scale[-1] - scale[0])
        relerror = abserror / yrange
        isoff = relerror > limit
        return abserror, isoff, relerror

    def check2d_linearity(self, xs, ys):
        """
        Check and warn the user with popups if the scales for the image plot is not linear.
        """
        # Alert if more than 0.5% off
        xabserror, xoff, xrelerror = ScanItem.check_linearity(xs, limit=0.005)
        yabserror, yoff, yrelerror = ScanItem.check_linearity(ys, limit=0.005)
        filename = os.path.split(self.filename)[1]

        if xoff and yoff:
            msg = "{}, {}: Both scales seems to be nonlinear. Image is displayed with linear scales," \
                " min to max. Data is {:.2f} ({:.2f} %) off in x-axis" \
                " and {:.2f} ({:.2f} %) off in y-axis.".format(
                    filename,
                    self.datasetname,
                    xabserror,
                    (100 * xrelerror),
                    yabserror,
                    (100 * yrelerror)
                )

            if self.log_signal is None:
                QtGui.QMessageBox.warning(None, "Warning", msg)
            else:
                self.log_signal.emit("WARN", msg)

        elif xoff:
            msg = "{}, {}: X-scale seems to be nonlinear. Image is displayed with linear scales," \
                " min to max. Data is {:.2f} ({:.2f} %) off in x-axis.".format(
                    filename,
                    self.datasetname,
                    xabserror,
                    (100 * xrelerror)
                )
            if self.log_signal is None:
                QtGui.QMessageBox.warning(None, "Warning", msg)
            else:
                self.log_signal.emit("WARN", msg)

        elif yoff:
            msg = "{}, {}: Y-scale seems to be nonlinear. Image is displayed with linear scales," \
                " min to max. Data is {:.2f} ({:.2f} %) off in y-axis.".format(
                    filename,
                    self.datasetname,
                    yabserror,
                    (100 * yrelerror)
                )
            if self.log_signal is None:
                QtGui.QMessageBox.warning(None, "Warning", msg)
            else:
                self.log_signal.emit("WARN", msg)

    def show3d(self):
        """
        Simply throw a popup displaying 3D data.
        """
        for threedidx in self.threeDscanidxs:
            with h5py.File(self.filename, "r", locking=False) as f:
                print("Launching a popup for 3D-data, this could take lots of memory.")
                threedref = self._columns[threedidx]
                data = f[threedref]
                imgplot = pg.image(data[...])

                def monkeyClose(s):
                    """
                    Cleanup hack. Cleans up most, but not all
                    """
                    imgplot.setImage(np.ones([2, 2]))
                    imgplot.deleteLater()

                imgplot.window().closeEvent = monkeyClose


class ScanDuplicate(ScanItem):
    """
    Simple duplicated scanitem
    """
    def __init__(self, original):
        if isinstance(original, ScanItem):
            filename = original.filename
            datasetname = original.datasetname+"_DUP"
        else:
            raise NotImplementedError
        original.load()
        ScanItem.__init__(self, filename, datasetname)
        self.setHeader(original.header)
        self.setColumns(original.columns)
        self.setXcolumnIndex(original.xColumnIndex)
        self.setYcolumnIndex(original.yColumnIndex)
        self.setYNorm(original.yNormConst)
        self.setUseScatter(original.useScatter)
        self.setTitle(original.title)

    def load(self):
        pass


class FileItem(CoreScanItem):
    """
    A fileItem is the container for ScanItems
    Fileitem makes the ScanItems appear in the correct file in treeview
    """
    def __init__(self, filename=None):
        self.scanList = []
        CoreScanItem.__init__(self, str(filename))
        head, tail = os.path.split(filename)
        if filename is None:
            self.setText("None")
        else:
            self.setText(tail)
        self.filename = filename
        self.settingsWidget = FileSettingsWidget(self, tail)
        self.settingsWidget.settingsChanged.connect(self.updatePlot)

    def cleanup(self):
        CoreScanItem.cleanup(self)
        self.scanList = []

    @property
    def plotItems(self):
        itms = []
        for scan in self.scanList:
            itms.extend(scan.plotItems)
        return itms

    @property
    def isDisplayed(self):
        ads = []
        for scan in self.scanList:
            ads.append(scan.isDisplayed)
        return any(ads)

    def setIsDisplayed(self, status):
        for scan in self.scanList:
            scan.setIsDisplayed(status)

    def setCheckState(self, state):
        CoreScanItem.setCheckState(self, state)
        for scan in self.scanList:
            scan.setCheckState(state)

    def fixCheckState(self):
        if all(item.checkState() == QtCore.Qt.Checked for item in self.scanList):
            CoreScanItem.setCheckState(self, QtCore.Qt.Checked)
        elif all(item.checkState() == QtCore.Qt.Unchecked for item in self.scanList):
            CoreScanItem.setCheckState(self, QtCore.Qt.Unchecked)
        else:
            CoreScanItem.setCheckState(self, 1)

    def setColor(self, color):
        for scan in self.scanList:
            scan.setColor(color)

    def appendRow(self, item):
        self.scanList.append(item)
        CoreScanItem.appendRow(self, [item, item.colorColumn])

    def getScanItem(self, datasetname):
        for scan in self.scanList:
            if scan.datasetname == datasetname:
                return scan

    def setUseScatter(self, yesno):
        for scan in self.scanList:
            scan.settingsWidget.scattercheck.setChecked(yesno)

    def setXcolumnIndex(self, index):
        for scan in self.scanList:
            scan.setXcolumnIndex(index)

    def setYcolumnIndex(self, index):
        for scan in self.scanList:
            scan.setYcolumnIndex(index)

    def setImgcolumnIndex(self, index):
        for scan in self.scanList:
            scan.setImgcolumnIndex(index)

    def updatePlot(self):
        for scan in self.scanList:
            scan.updatePlot()


class ScanModel(QtGui.QStandardItemModel):
    """
    ScanModel interfaces from treeview with fileitems and scanitems
    """
    def __init__(self, parent=None):
        QtGui.QStandardItemModel.__init__(self, 0, 1)
        self.clear()

    def clear(self):
        """
        Clear the scanmodel, set the columns and clear the filelist
        """
        QtGui.QStandardItemModel.clear(self)
        self.setColumnCount(2)
        self.setHeaderData(0, QtCore.Qt.Horizontal, Qt.QVariant("Scan"))
        self.setHeaderData(1, QtCore.Qt.Horizontal, Qt.QVariant(""))
        self.fileList = []

    def appendRow(self, item):
        """
        Append an item and its colorcolumn. item is usually a file, but can also be a specialitem e.g. mathitem or
        liveitem.
        """
        if isinstance(item, FileItem):
            self.fileList.append(item)
        QtGui.QStandardItemModel.appendRow(self, [item, item.colorColumn])

    def isFileLoaded(self, filename):
        """
        Returns file if file is loaded.
        """
        for fileitem in self.fileList:
            if fileitem.filename == filename:
                return fileitem

    def getFileItem(self, filename):
        """
        If file is loaded return fileitem, otherwise add and return new fileitem
        """
        fileitem = self.isFileLoaded(filename)
        if fileitem is None:
            fileitem = FileItem(filename)
            self.appendRow(fileitem)
        return fileitem
