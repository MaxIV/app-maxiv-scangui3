import numpy as np
import pyqtgraph as pg

from .scanitem import CoreScanItem
from .widgets.roisettings import RoiSettingsWidget
from ..utils.pyqtgraphitems import EmptyErrorBarItem


class RoiItem(CoreScanItem):
    """
    Tool that allows user to sum up a spectrum from an image.
    """
    name = "Image line sum tool"

    def __init__(self, parentScan):
        self.parentScan = parentScan
        title = parentScan.text()+"_ROI"
        CoreScanItem.__init__(self, title)
        pen = pg.mkPen(color=(255, 0, 0), width=3)

        self.datasetname = title
        self.xColumnIndex = 0
        self.yColumnIndex = 1
        self.yNormConst = 1
        self.is2d = False

        try:
            xscale = np.asarray(self.parentScan.getColumn(self.parentScan.xColumnIndex), dtype=float)
            yscale = np.asarray(self.parentScan.getColumn(self.parentScan.yColumnIndex), dtype=float)
            lo = [xscale[0], yscale[0]]
            hi = [xscale[-1], yscale[-1]]
        except Exception as er:
            lo = [0, 1]
            hi = [20, 21]
            print(er)

        self.roirect = pg.LineSegmentROI(positions=(lo, hi), pen=pen)
        self.addPlotItem(self.roirect)
        self.roirect.sigRegionChanged.connect(self.refreshRoi)

        self.yplotPopup = pg.PlotWidget(pen=(255, 0, 0))
        self.yplotPopup.setWindowTitle("Summed along X-axis")
        self.yplotPopup.plotItem.setTitle("Summed along X-axis")
        self.yplot = pg.PlotDataItem(pen=(0, 255, 0))
        self.yplotPopup.addItem(self.yplot)
        self.yplotPopup.show()
        self.ylimitsplot = EmptyErrorBarItem(pen=(0, 255, 0))
        self.addPlotItem(self.ylimitsplot)

        self.xplotPopup = pg.PlotWidget()
        self.xplotPopup.setWindowTitle("Summed along Y-axis")
        self.xplotPopup.plotItem.setTitle("Summed along Y-axis")
        self.xplot = pg.PlotDataItem()
        self.xplotPopup.addItem(self.xplot)
        self.xplotPopup.show()
        self.xlimitsplot = EmptyErrorBarItem(pen=(255, 0, 0))
        self.addPlotItem(self.xlimitsplot)

        self.settingsWidget = RoiSettingsWidget(self, title)
        self.settingsWidget.settingsChanged.connect(self.updatePlot)

        self.showx = True
        self.showy = False

        self.ynorm = 1
        self.xnorm = 1
        self.width = 100  # Channels

        self.xs = np.array([])
        self.ys = np.array([])

    def getColumn(self, columindex):
        """
        Implemented so that one can do math on mathitems.
        """
        if self.showx and self.showy:
            raise TypeError("Cannot do math when summing along both axis at the same time")
        if columindex == self.xColumnIndex:
            return self.xs
        if columindex == self.yColumnIndex:
            return self.ys

    def cleanup(self):
        """
        Close the popups when removing the scanitem
        """
        self.xplotPopup.hide()
        self.yplotPopup.hide()
        CoreScanItem.cleanup(self)

    def activate(self):
        """
        Activate the plot settings, also show the popups
        """
        CoreScanItem.activate(self)
        if self.showx:
            self.xplotPopup.show()
            self.xplotPopup.raise_()
        if self.showy:
            self.yplotPopup.show()
            self.yplotPopup.raise_()

    def set_ynorm(self, newnorm):
        self.ynorm = newnorm
        self.updatePlot()

    def set_xnorm(self, newnorm):
        self.xnorm = newnorm
        self.updatePlot()

    def set_width(self, newwidth):
        self.width = newwidth-1
        self.updatePlot()

    def refreshRoi(self, roi):
        """
        Use the roi tool, get the line from underneath it,
        For every x-step, sum up the y-values that are within width.
        This will display how the image has changed under the line.
        Then do the same with the image flipped.
        """
        if not self.parentScan.isDisplayed:
            raise Exception("Source scan, "+str(self.parentScan.datasetname)+", needs to be in plot")
        if not self.parentScan.is2d:
            raise Exception("Source scan, "+str(self.parentScan.datasetname)+", is not 2 dimensional")

        handles = roi.getLocalHandlePositions()
        x0 = handles[0][1].x()+roi.pos().x()
        x1 = handles[1][1].x()+roi.pos().x()
        y0 = handles[0][1].y()+roi.pos().y()
        y1 = handles[1][1].y()+roi.pos().y()

        # Getting x and y-scale
        xscale = np.asarray(self.parentScan.getColumn(self.parentScan.xColumnIndex), dtype=float)
        yscale = np.asarray(self.parentScan.getColumn(self.parentScan.yColumnIndex), dtype=float)

        if not self.showx:
            self.xplotPopup.hide()

        if not self.showy:
            self.yplotPopup.hide()

        if self.parentScan.xColumnIndex == -1:
            xscale = np.arange(0, self.parentScan.imageData.shape[0])
        if self.parentScan.yColumnIndex == -1:
            yscale = np.arange(0, self.parentScan.imageData.shape[1])

        if len(xscale) != self.parentScan.imageData.shape[0]:
            raise Exception("X-scale has the wrong length ("+str(len(xscale))+") for tool.\n" +
                            "Should be "+str(self.parentScan.imageData.shape[0]))
        if len(yscale) != self.parentScan.imageData.shape[1]:
            raise Exception("Y-scale has the wrong length ("+str(len(yscale))+") for tool")

        image = self.parentScan.imageData
        width = self.width
        if self.showx:
            result = RoiItem._calculate_roi(image, width, xscale, yscale, x0, x1, y0, y1, 1)
            if result is not None:
                xvals, y, xspectrum, top, bottom, beamwidth = result
                self.xs = xvals
                self.ys = xspectrum
                self._draw_x_roi(xvals, y, xspectrum, top, bottom, beamwidth)
        if self.showy:
            result = RoiItem._calculate_roi(image, width, yscale, xscale, y0, y1, x0, x1, 0)
            if result is not None:
                yvals, x, yspectrum, right, left, beamwidth = result
                self.xs = yvals
                self.ys = yspectrum
                self._draw_y_roi(yvals, x, yspectrum, left, right, beamwidth)

        # Hack to fix so that the roi handles (circles) doesn't reshape when moved
        try:
            for handle in roi.handles:
                handle['item'].viewTransformChanged()
        except Exception as er:
            print(er)

    def _draw_x_roi(self, xvals, yvals, xspectrum, top, bottom, beamwidth):
        self.xlimitsplot.setOpts(x=xvals, y=yvals, top=top, bottom=bottom, beam=beamwidth)
        self.xplot.setData(xvals, xspectrum/self.xnorm)
        self.xplotPopup.show()

    def _draw_y_roi(self, yvals, xvals, yspectrum, left, right, beamwidth):
        self.ylimitsplot.setOpts(x=xvals, y=yvals, left=left, right=right, beam=beamwidth)
        self.yplot.setData(yspectrum/self.ynorm, yvals)
        self.yplotPopup.show()

    @staticmethod
    def _calculate_roi(image, width, axisscale, valuescale, axislow, axishigh, lowvalue, highvalue, dimension):
        """
        Calcualtes the output of the roi item.
        """
        def find_nearest(array, value):
            # Returns nearest index of value in array
            return (np.abs(array-value)).argmin()

        # Making sure we are working on monotonic
        if axislow > axishigh:
            axislow, axishigh = axishigh, axislow
            lowvalue, highvalue = highvalue, lowvalue

        # Going from scaled to idxs
        lowaxisidx = find_nearest(axisscale, axislow)
        highaxisidx = find_nearest(axisscale, axishigh)
        lowvalueidx = find_nearest(valuescale, lowvalue)
        highvalueidx = find_nearest(valuescale, highvalue)

        # Creating all idxs.
        axisidxs = np.arange(lowaxisidx, highaxisidx+1)
        valueidxs = np.linspace(lowvalueidx, highvalueidx, len(axisidxs)).astype(int)
        if len(axisidxs) > 1:
            # Summing along the image
            spectrum, loedge, hiedge = RoiItem._sum_along_image(image, axisidxs, valueidxs, width, dimension)

            # Going back from idxs to scaled
            axisvalues = axisscale[axisidxs]
            valuevalues = valuescale[valueidxs]
            spectrum = np.array(spectrum)
            diffr = np.mean(np.diff(valuescale)) / 2  # Adding a half step for visualisation on image

            # Finding high, low and beamwidth for error bar item
            highbar = valuescale[hiedge] + diffr - valuevalues
            lowbar = valuevalues - valuescale[loedge] + diffr
            beamwidth = np.mean(np.diff(axisvalues))
            return axisvalues, valuevalues, spectrum, highbar, lowbar, beamwidth

    @staticmethod
    def _sum_along_image(image, idxs, valueidxs, width, dimension):
        """
        Sum along idxs and valueidxs with width.
        idxs can be e.g. xidxs and valueidxs can be yidxs or vice versa
        Dimension 0 will sum along x-axis, and dimension 1 will sum along y-axis
        Also returns lo and higgh edges of where it has summed the spectrum
        """
        spectrum = []
        loedge = []
        hiedge = []
        for idx, valueidx in zip(idxs, valueidxs):
            minvalidx = np.max([valueidx-width/2, 0])
            loedge.append(minvalidx)
            maxvalidx = np.min([valueidx+width/2, image.shape[dimension]-1])
            hiedge.append(maxvalidx)
            if dimension == 0:
                slicesum = np.sum(np.flipud(image)[minvalidx:maxvalidx+1, idx])
            elif dimension == 1:
                slicesum = np.sum(np.flipud(image)[idx, minvalidx:maxvalidx+1])
            else:
                raise Exception("Invalid dimension")
            spectrum.append(slicesum)
        return spectrum, loedge, hiedge

    def updatePlot(self):
        """
        Replot!
        """
        self.refreshRoi(self.roirect)

    def setColor(self, color):
        """
        Set color of plot.
        """
        CoreScanItem.setColor(self, color)
        pen = pg.mkPen(color)
        self.xplot.setPen(pen)
        self.yplot.setPen(pen)
