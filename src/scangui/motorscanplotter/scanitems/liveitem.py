import numpy as np

from .scanitem import ScanItem


class LiveItem(ScanItem):
    """
    A live scanitem that listens to the doors recorddata variable.
    JSONrecorder needs to be enabled in sardana.
    """

    def __init__(self):
        ScanItem.__init__(self, "Livefile", "Livedata")
        self.doorName = None
        self._records = []
        self._values = []
        self._name_label = {}
        self._lastpoint = 1  # Is 1 so a new scan will look like new data

    def load(self):
        pass

    def setDoorName(self, doorname):
        self.doorName = doorname

    def _change_names_to_labels(self, record):
        """
        Use the dictionary to translate the tango names into its labels
        """
        for item in list(record):
            if item in self._name_label:
                newkey = self._name_label[item]
                record[newkey] = record.pop(item)
        return record

    def setScanData(self, new_record):
        """
        Sets the data in scanitem to make it plottable
        """
        for col_num, value in enumerate(new_record.values()):
            column = np.append(self._values[col_num], value)
            self._values[col_num] = column
        self.setColumns(self._values)

    def onRecordDataUpdated(self, args):
        """
        Gives us the data from the qdoor. Take the data and add it to the liveplot,
        if the point number is lower than the last point number then we conclude that it is a new scan.
        Also takes some special data to make the gui nicer:
         column_desc, is pushed when a new scan is started, it gives nice names of the attributes instead of just
                      the device names
         title, gives us the name of the macro, i.e. the string of the macro launched
        """
        try:
            data = args[0][1]['data']
        except Exception as er:
            print(f"Exception getting data from {args}: {er}")
            return
        try:
            if 'column_desc' in data:
                # This gives a description with nice labels combined with the names,
                # By using it we can get nice names instead of tango names in the plot settings
                self._name_label = {}
                column_desc = data['column_desc']
                for item in column_desc:
                    label = item['label']
                    name = item['name']
                    if name not in self._name_label:
                        self._name_label[name] = label
        except Exception as er:
            print("Exception getting column description: " + str(er))

        if 'point_nb' in data:
            # This has a point, then this is a record
            record = data
            point = record['point_nb']
            record = self._change_names_to_labels(record)

            columns_changed = True
            current_columns = record.keys()
            if self._records:
                last_record = self._records[-1]
                existing_columns = last_record.keys()
                columns_changed = existing_columns != current_columns
                new_scan = point < self._lastpoint
                if new_scan or columns_changed:
                    self._records = []
                    self._values = [np.empty((0,)) for _ in current_columns]
                    

            if columns_changed:
                self.setHeader(list(current_columns))
                self._values = [np.empty((0,)) for _ in current_columns]
            self._records.append(record)
            self.setScanData(record)

            if point < self._lastpoint:
                self.settingsWidget.activate()  # Update plot settings widget
            self.updatePlot()

            if point > self._lastpoint + 1 and not columns_changed:  # Error checking
                lastpoint = self._lastpoint
                self._lastpoint = point
                # The exception here is hidden because if sequencing is used it will trigger all the time
                print("It seems like the live plot is skipping points, went from " + str(lastpoint) + " to " + str(
                    point) + ".")
            self._lastpoint = point

        if 'title' in data:
            title = str(data['title'])
            self.setTitle(title)
            print(title)