from taurus.external.qt import QtCore, QtGui

from .settings import CoreSettingsWidget


class MathSettingsWidget(CoreSettingsWidget):
    """
    A settings widget for the math items. Has a simple check to set if it is a line plot and scatter plot
    Also contains a button to redo the calculations, which is supposed to be pressed if one of the scans where changed.
    """
    def __init__(self, scan, title):
        CoreSettingsWidget.__init__(self, scan, title)
        l4 = QtGui.QLabel("Scatter:")
        self.scattercheck = QtGui.QCheckBox()
        self.scattercheck.setCheckState(QtCore.Qt.Checked)

        formlayout = QtGui.QFormLayout()
        self.layout().addLayout(formlayout)
        formlayout.addRow(l4, self.scattercheck)

        self.scattercheck.stateChanged.connect(self.scatterCheckChanged)

        self.updateButton = QtGui.QPushButton("Refresh calculations")
        self.layout().addWidget(self.updateButton)
        self.updateButton.clicked.connect(self.updateButtonClicked)

    def updateButtonClicked(self):
        """
        Redo calculations and replot.
        """
        self.scan.updatePlot()

    def scatterCheckChanged(self, state):
        """
        Change between scatterplot and curveplot
        """
        if state == QtCore.Qt.Checked:
            self.scan.setUseScatter(True)
        elif state == QtCore.Qt.Unchecked:
            self.scan.setUseScatter(False)