from taurus.external.qt import Qt, QtCore, QtGui

class CoreSettingsWidget(QtGui.QWidget):
    settingsChanged = QtCore.pyqtSignal()

    def __init__(self, scan, title):
        QtGui.QWidget.__init__(self)
        self.scan = scan
        self.setLayout(QtGui.QVBoxLayout(self))
        label = QtGui.QLabel(title)
        self.layout().addWidget(label)

    def activate(self):
        """
        This is called when the widget is displayed in the gui.
        Use this to e.g. populate comboboxes.
        """
        pass

    def applySettings(self):
        """
        Connect to tell the plot that you want it to be updated.
        """
        self.settingsChanged.emit()

class ScanSettingsWidget(CoreSettingsWidget):
    """
    Settings widget for a normal scan item, includes comboboxes to set which data should appear on which axis
    """
    def __init__(self, scan, title):
        CoreSettingsWidget.__init__(self, scan, title)
        formlayout = QtGui.QFormLayout()
        self.layout().addLayout(formlayout)

        l1 = QtGui.QLabel("X axis")
        self.comboXdata = QtGui.QComboBox()
        formlayout.addRow(l1, self.comboXdata)

        l2 = QtGui.QLabel("Y axis")
        self.comboYdata = QtGui.QComboBox()
        formlayout.addRow(l2, self.comboYdata)

        self.comboImgdataLabel = QtGui.QLabel("Img axis")
        self.comboImgdata = QtGui.QComboBox()
        self.imgTransposeCheck = QtGui.QCheckBox("T")
        self.comboImgdataLabel.hide()
        self.comboImgdata.hide()
        self.imgTransposeCheck.hide()
        imglay = QtGui.QHBoxLayout()
        imglay.addWidget(self.comboImgdata, 2)
        imglay.addWidget(self.imgTransposeCheck)
        formlayout.addRow(self.comboImgdataLabel, imglay)

        l4 = QtGui.QLabel("Norm: Y/")
        self.yNormLine = QtGui.QLineEdit("1")
        formlayout.addRow(l4, self.yNormLine)

        l5 = QtGui.QLabel("Scatter:")
        self.scattercheck = QtGui.QCheckBox()
        self.scattercheck.setCheckState(QtCore.Qt.Unchecked)
        formlayout.addRow(l5, self.scattercheck)

        self.threeDlabel = QtGui.QLabel("3D:")
        self.threeDbutton = QtGui.QPushButton("Display")
        formlayout.addRow(self.threeDlabel, self.threeDbutton)
        self.threeDlabel.hide()
        self.threeDbutton.hide()

        self.threeDbutton.clicked.connect(self.onThreeDbuttonClicked)
        self.scattercheck.stateChanged.connect(self.scatterCheckChanged)
        self.imgTransposeCheck.stateChanged.connect(self.imgTransposeChanged)
        self.yNormLine.textChanged.connect(self.onNormChanged)
        self.comboXdata.activated.connect(self.onPlotComboActivated)
        self.comboYdata.activated.connect(self.onPlotComboActivated)
        self.comboImgdata.activated.connect(self.onPlotComboActivated)

    def activate(self):
        """
        Called when raised to be shown
        """
        self.populate()

    def populate(self):
        """
        Populate the settingswidgets comboboxes with scans.
        """
        self.comboXdata.clear()
        self.comboYdata.clear()
        self.comboImgdata.clear()
        comboitems = [self.scan.header[i] for i in self.scan.oneDscanidxs]
        try:
            oldXindex = self.scan.oneDscanidxs.index(self.scan.xColumnIndex)
        except:
            oldXindex = -1
        try:
            oldYindex = self.scan.oneDscanidxs.index(self.scan.yColumnIndex)
        except:
            oldYindex = -1
        if self.scan.hastwoDdata():
            self.comboXdata.addItem("-", -1)
            self.comboYdata.addItem("-", -1)
            oldXindex += 1
            oldYindex += 1
        for label, idx in zip(comboitems, self.scan.oneDscanidxs):
            self.comboXdata.addItem(label, idx)
            self.comboYdata.addItem(label, idx)

        # ugly ugly
        if oldXindex == -1:
            oldXindex = 0
        if oldYindex == -1:
            oldYindex = 0

        self.comboXdata.setCurrentIndex(oldXindex)
        self.comboYdata.setCurrentIndex(oldYindex)
        self.comboXdata.setEnabled(True)
        self.comboYdata.setEnabled(True)
        if self.scan.hastwoDdata():
            self.comboImgdata.show()
            self.comboImgdataLabel.show()
            self.imgTransposeCheck.show()
        else:
            self.comboImgdata.hide()
            self.comboImgdataLabel.hide()
            self.imgTransposeCheck.hide()
        if self.scan.hasthreeDdata():
            self.threeDbutton.show()
            self.threeDlabel.show()
        else:
            self.threeDbutton.hide()
            self.threeDlabel.hide()
        self.comboImgdata.addItem("", -1)
        imgcomboitems = [self.scan.header[i] for i in self.scan.twoDscanidxs]
        try:
            oldImgindex = self.scan.twoDscanidxs.index(self.scan.imgColumnIndex) + 1
        except:
            oldImgindex = -1
        # ugly ugly
        if oldImgindex == -1:
            oldImgindex = 0

        for label, idx in zip(imgcomboitems, self.scan.twoDscanidxs):
            self.comboImgdata.addItem(label, idx)
        self.comboImgdata.setCurrentIndex(oldImgindex)

    def onPlotComboActivated(self):
        """
        This is called when the plot data settings are changed.
        """
        xidx = self.comboXdata.currentIndex()
        yidx = self.comboYdata.currentIndex()
        imgidx = self.comboImgdata.currentIndex()
        xcolumn = self.comboXdata.itemData(xidx)
        ycolumn = self.comboXdata.itemData(yidx)
        imgcolumn = self.comboImgdata.itemData(imgidx)
        self.scan.setXcolumnIndex(xcolumn)
        self.scan.setYcolumnIndex(ycolumn)
        self.scan.setImgcolumnIndex(imgcolumn)
        self.applySettings()

    def onNormChanged(self):
        """
        This is called when the plot norm settings are changed.
        """
        if self.yNormLine.text() == "":
            return
        normConst = float(self.yNormLine.text())
        self.scan.setYNorm(normConst)
        self.applySettings()

    def scatterCheckChanged(self, state):
        if state == QtCore.Qt.Checked:
            self.scan.setUseScatter(True)
        elif state == QtCore.Qt.Unchecked:
            self.scan.setUseScatter(False)

    def imgTransposeChanged(self, state):
        if state == QtCore.Qt.Checked:
            self.scan.setImgTranspose(True)
        elif state == QtCore.Qt.Unchecked:
            self.scan.setImgTranspose(False)

    def onThreeDbuttonClicked(self):
        self.scan.show3d()


class FileSettingsWidget(CoreSettingsWidget):
    """
    Settings widget for a fileitem
    """
    def __init__(self, fileitem, title):
        CoreSettingsWidget.__init__(self, fileitem, title)
        self.file = fileitem
        formlayout = QtGui.QFormLayout()
        self.layout().addLayout(formlayout)

        l1 = QtGui.QLabel("X column")
        self.comboXdata = QtGui.QComboBox()
        formlayout.addRow(l1, self.comboXdata)

        l2 = QtGui.QLabel("Y column")
        self.comboYdata = QtGui.QComboBox()
        formlayout.addRow(l2, self.comboYdata)

        l4 = QtGui.QLabel("Scatter/Line")
        self.scattercheck = QtGui.QCheckBox()
        self.scattercheck.setCheckState(QtCore.Qt.Unchecked)
        formlayout.addRow(l4, self.scattercheck)

        self.comboImgdataLabel = QtGui.QLabel("Img axis")
        self.comboImgdata = QtGui.QComboBox()
        self.comboImgdataLabel.hide()
        self.comboImgdata.hide()
        imglay = QtGui.QHBoxLayout()
        imglay.addWidget(self.comboImgdata, 2)
        formlayout.addRow(self.comboImgdataLabel, imglay)

        self.scattercheck.stateChanged.connect(self.scatterCheckChanged)

        self.comboXdata.activated.connect(self.onPlotComboActivated)
        self.comboYdata.activated.connect(self.onPlotComboActivated)
        self.comboImgdata.activated.connect(self.onPlotComboActivated)

    def activate(self):
        self.populate()

    def populate(self):
        """
        This populates the plot settings corresponding for a fileitem.
        It takes the header count from the last scan in the fileitem
        """
        self.comboXdata.clear()
        self.comboYdata.clear()
        self.comboImgdata.clear()
        if len(self.file.scanList) > 0:
            lastScan = self.file.scanList[-1]
            comboitems = [lastScan.header[i] for i in lastScan.oneDscanidxs]
            lastScan.settingsWidget.populate()
            comboxidx = lastScan.settingsWidget.comboXdata.currentIndex()
            comboyidx = lastScan.settingsWidget.comboYdata.currentIndex()
            comboimgidx = lastScan.settingsWidget.comboImgdata.currentIndex()
            if lastScan.hastwoDdata():
                self.comboXdata.addItem("-", -1)
                self.comboYdata.addItem("-", -1)
            for label, idx in zip(comboitems, lastScan.oneDscanidxs):
                self.comboXdata.addItem(label, idx)
                self.comboYdata.addItem(label, idx)
            if lastScan.hastwoDdata():
                self.comboImgdata.show()
                self.comboImgdataLabel.show()
            else:
                self.comboImgdata.hide()
                self.comboImgdataLabel.hide()
            self.comboImgdata.addItem("", -1)
            imgcomboitems = [lastScan.header[i] for i in lastScan.twoDscanidxs]
            for label, idx in zip(imgcomboitems, lastScan.twoDscanidxs):
                self.comboImgdata.addItem(label, idx)
            self.comboXdata.setCurrentIndex(comboxidx)
            self.comboYdata.setCurrentIndex(comboyidx)
            self.comboImgdata.setCurrentIndex(comboimgidx)
        self.comboXdata.setEnabled(True)
        self.comboYdata.setEnabled(True)
        self.comboImgdata.setEnabled(True)

    def onPlotComboActivated(self):
        """
        This is called when the plot data settings are changed.
        """
        xidx = self.comboXdata.currentIndex()
        yidx = self.comboYdata.currentIndex()
        imgidx = self.comboImgdata.currentIndex()
        xcolumn = self.comboXdata.itemData(xidx)
        ycolumn = self.comboXdata.itemData(yidx)
        imgcolumn = self.comboImgdata.itemData(imgidx)
        self.file.setXcolumnIndex(xcolumn)
        self.file.setYcolumnIndex(ycolumn)
        self.scan.setImgcolumnIndex(imgcolumn)
        self.applySettings()

    def scatterCheckChanged(self, state):
        if state == QtCore.Qt.Checked:
            self.file.setUseScatter(True)
        elif state == QtCore.Qt.Unchecked:
            self.file.setUseScatter(False)