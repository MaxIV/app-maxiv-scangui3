from taurus.external.qt import QtCore, QtGui

from .settings import CoreSettingsWidget

class RoiSettingsWidget(CoreSettingsWidget):
    """
    Settings widget for the roi spectrum tool, where the user can select what is supposed to be
    displayed in th plotwidget. And also normalize it with constants.
    """
    def __init__(self, scan, title):
        CoreSettingsWidget.__init__(self, scan, title)
        formlayout = QtGui.QFormLayout()
        self.layout().addLayout(formlayout)
        l1 = QtGui.QLabel("Sum along Y axis")
        self.xcheck = QtGui.QCheckBox()
        self.xcheck.setCheckState(QtCore.Qt.Checked)
        formlayout.addRow(l1, self.xcheck)
        self.xcheck.stateChanged.connect(self.xCheckChanged)

        l2 = QtGui.QLabel("Sum along X axis")
        self.ycheck = QtGui.QCheckBox()
        self.ycheck.setCheckState(QtCore.Qt.Unchecked)
        formlayout.addRow(l2, self.ycheck)
        self.ycheck.stateChanged.connect(self.yCheckChanged)

        l4 = QtGui.QLabel("Tool")
        self.toolcheck = QtGui.QCheckBox()
        self.toolcheck.setCheckState(QtCore.Qt.Checked)
        formlayout.addRow(l4, self.toolcheck)
        self.toolcheck.stateChanged.connect(self.toolCheckChanged)

        l3 = QtGui.QLabel("Width(ch)")
        self.widthLine = QtGui.QLineEdit("100")
        formlayout.addRow(l3, self.widthLine)
        self.widthLine.editingFinished.connect(self.widthEdited)

        l6 = QtGui.QLabel("X plot /")
        self.xNormLine = QtGui.QLineEdit("1")
        formlayout.addRow(l6, self.xNormLine)
        self.xNormLine.editingFinished.connect(self.xNormLineEdited)

        l5 = QtGui.QLabel("Y plot /")
        self.yNormLine = QtGui.QLineEdit("1")
        formlayout.addRow(l5, self.yNormLine)
        self.yNormLine.editingFinished.connect(self.yNormLineEdited)

    def widthEdited(self):
        """
        The width of sum region is changed
        """
        newstr = self.widthLine.text()
        width = float(newstr)
        self.scan.set_width(width)

    def yNormLineEdited(self):
        """
        Normalization constant for yplot changed
        """
        newstr = self.yNormLine.text()
        yNorm = float(newstr)
        self.scan.set_ynorm(yNorm)

    def xNormLineEdited(self):
        """
        Normalization constant for xplot changed
        """
        newstr = self.xNormLine.text()
        xNorm = float(newstr)
        self.scan.set_xnorm(xNorm)

    def xCheckChanged(self, state):
        """
        Checked means that the user wants to sum along y axis,
        Display or hide apropriate plotitems
        """
        if state == QtCore.Qt.Checked:
            self.scan.showx = True
        elif state == QtCore.Qt.Unchecked:
            self.scan.xlimitsplot.clear()
            self.scan.xplot.clear()
            self.scan.showx = False
        self.applySettings()

    def yCheckChanged(self, state):
        """
        Checked means that the user wants to sum along x axis
        """
        if state == QtCore.Qt.Checked:
            self.scan.showy = True
        elif state == QtCore.Qt.Unchecked:
            self.scan.ylimitsplot.clear()
            self.scan.yplot.clear()
            self.scan.showy = False
        self.applySettings()

    def toolCheckChanged(self, state):
        """
        Hide the tools and limits
        """
        if state == QtCore.Qt.Checked:
            self.scan.roirect.show()
        elif state == QtCore.Qt.Unchecked:
            self.scan.roirect.hide()
            self.scan.xlimitsplot.clear()
            self.scan.ylimitsplot.clear()
