import numpy as np
import pyqtgraph as pg

from .scanitem import CoreScanItem
from .widgets.mathsettings import MathSettingsWidget


class MathItem(CoreScanItem):
    """
    Generic mathscanitem which can do math expressions between two scanitems.
    """
    name = "Noname"

    def __init__(self, scan1, scan2, expressionstr):
        self.scan1 = scan1
        self.scan2 = scan2
        title = str(scan1.datasetname)+expressionstr+str(scan2.datasetname)
        CoreScanItem.__init__(self, title)
        self.settingsWidget = MathSettingsWidget(self, title)

        self.useScatter = True

        self.plotitem = pg.PlotDataItem(pen=None, symbol='o')
        self.addPlotItem(self.plotitem)
        self.plotitem.sigPointsClicked.connect(self.onPointClicked)

        self.datasetname = title
        self.xColumnIndex = 0
        self.yColumnIndex = 1

        self.is2d = False
        self.yNormConst = 1
        self.xs = np.array([])
        self.ys = np.array([])

    def getColumn(self, columindex):
        """
        Implemented so that one can do math on mathitems.
        """
        if columindex == self.xColumnIndex:
            return self.xs
        if columindex == self.yColumnIndex:
            return self.ys

    def expression(self, y1, y2):
        """
        y1 and y2 are the y values that are closest to each other on the x-axis
        override this function to create different mathematics funtions

        e.g. return y1/y2
        """
        raise NotImplementedError

    def calculate(self):
        """
        Do the calculations for the mathitem
        """
        xs = np.copy(np.asarray(self.scan1.getColumn(self.scan1.xColumnIndex), dtype=float))
        ys = np.copy(np.asarray(self.scan1.getColumn(self.scan1.yColumnIndex), dtype=float))
        if self.scan1.yNormConst:
            ys = ys/self.scan1.yNormConst
        scan2x = np.copy(np.asarray(self.scan2.getColumn(self.scan2.xColumnIndex), dtype=float))
        scan2y = np.copy(np.asarray(self.scan2.getColumn(self.scan2.yColumnIndex), dtype=float))
        if self.scan2.yNormConst:
            scan2y = scan2y/self.scan2.yNormConst
        for i in range(0, len(xs)):
            x = xs[i]
            y = ys[i]
            yIndex = self.find_nearest(scan2x, x)
            ys[i] = self.expression(y, scan2y[yIndex])
        self.xs = xs
        self.ys = ys

    def updatePlot(self):
        """
        This does the caluclations. If the x-values are not the same then it will use the point from scan2 with
         x closest to the point in scan 1. e.g. self.find_nearest(scan2x,x)
        """
        self.calculate()
        self.plotitem.setData(self.xs, self.ys)

    def setColor(self, color):
        """
        Set the color for the mathitem.
        """
        CoreScanItem.setColor(self, color)
        pen = pg.mkPen(color)
        brush = pg.mkBrush(color)

        if self.useScatter:
            self.plotitem.setPen(None)
            self.plotitem.setSymbol('o')
            self.plotitem.setSymbolPen(pen)
            self.plotitem.setSymbolBrush(brush)
        else:
            self.plotitem.setSymbol(None)
            self.plotitem.setPen(pen)

    def setUseScatter(self, yesno):
        """
        Change between using scatterplot and not, keep the color.
        """
        self.useScatter = yesno
        if self.useScatter:
            self.plotitem.setPen(None)
            self.plotitem.setSymbol('o')
        else:
            self.plotitem.setSymbol(None)
            self.plotitem.setPen(self.plotitem.opts['symbolPen'])

    @staticmethod
    def find_nearest(array, value):
        """
        Helper function when comparing. Finds the nearest index of value in array.
        """
        idx = (np.abs(array-value)).argmin()
        return idx


class DivisionItem(MathItem):
    name = "Divide"

    def __init__(self, scan1, scan2):
        MathItem.__init__(self, scan1, scan2, "/")

    def expression(self, y1, y2):
        return y1/y2


class MultiplyItem(MathItem):
    name = "Muliply"

    def __init__(self, scan1, scan2):
        MathItem.__init__(self, scan1, scan2, "*")

    def expression(self, y1, y2):
        return y1*y2


class AdditionItem(MathItem):
    name = "Addition"

    def __init__(self, scan1, scan2):
        MathItem.__init__(self, scan1, scan2, "+")

    def expression(self, y1, y2):
        return y1+y2


class SubtractItem(MathItem):
    name = "Subtraction"

    def __init__(self, scan1, scan2):
        MathItem.__init__(self, scan1, scan2, "-")

    def expression(self, y1, y2):
        return y1-y2


class NormItem(MathItem):
    """
    Divides scan1 with maximum of scan2
    typical usage where scan1==scan2
    """
    name = "scan1/Max(scan2)"

    def __init__(self, scan1, scan2):
        MathItem.__init__(self, scan1, scan2, "/max: ")

    def expression(self, y1, y2):
        scan2max = np.max(np.asarray(self.scan2.getColumn(self.scan2.yColumnIndex)))
        return y1/scan2max
