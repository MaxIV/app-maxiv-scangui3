import json
import os
import sys

import pyqtgraph as pg
import taurus
from taurus.external.qt import Qt, QtCore, QtGui, compat
from taurus.qt.qtgui.container import TaurusWidget
from watchdog.observers.polling import PollingObserver

from .backend import Backend
from .scanitems import liveitem, mathitems, roitool, scanitem
from .ui_motorscanplotter import Ui_Form
from .utils.addspecialpopup import AddSpecialDialog
from .utils.qtwatchdog import QtFileEventHandler
import logging

class Motorscanplotter(TaurusWidget):
    """
    Scanplotter creates a pyqtgraph plotwidget and a treeview.
    Scanplotter listens to macrostatusupdated from a qdoor
    When data recieves, liveitem polls data from door recordData
    When file is updated scanplotter loads the h5 file
    """
    experimentConfigurationChanged = Qt.pyqtSignal(compat.PY_OBJECT)

    def __init__(self, parent=None, designMode=False):
        TaurusWidget.__init__(self, parent, designMode=designMode)
        self._ui = Ui_Form()
        self._ui.setupUi(self)
        self.plotWidget = pg.PlotWidget()
        self._ui.plotLayout.addWidget(self.plotWidget)
        self.addCrosshair()
        self.setupQtConnections()
        self.backend = Backend()

        self._ui.treeView.setModel(self.backend.scanModel)
        self._ui.treeView.setColumnWidth(0, 226)
        self._ui.treeView.setColumnWidth(1, 5)

        self.backend.scanSelected.connect(self.onObjectSelected)
        self.backend.newScanAdded.connect(self.onNewScanLoaded)
        self.levelControlItem = pg.HistogramLUTItem()

        self.backend.addLiveItem()
        self.doorName = None
        self.sardana_save_path = None
        self.sardana_file = None

        self.registerConfigProperty("saveSettingsToStr", "readSettingsFromStr", "motorscanplotter")

        # WATCHDOG FOR FILE
        self.file_event_handler = QtFileEventHandler()
        self.file_event_handler.file_created.connect(self.on_file_watchdog_event)
        self.file_event_handler.file_modified.connect(self.on_file_watchdog_event)
        self.setLogLevel(logging.DEBUG)
        self.log_signal = None

    def setupQtConnections(self):
        """
        Qt stuff setup internal connections.
        """
        self.debug('setupQtConnections')
        self._ui.loadRefButton.clicked.connect(self.onLoadRefButtonClicked)
        self._ui.reloadButton.clicked.connect(self.onReloadButtonClicked)
        self._ui.unloadButton.clicked.connect(self.onUnloadButton)
        self._ui.autoLoad.stateChanged.connect(self.autoLoadChanged)
        self._ui.addSpecialButton.clicked.connect(self.onAddSpecialButton)
        self._ui.duplicateButton.clicked.connect(self.onDuplicateButton)
        self._ui.removeButton.clicked.connect(self.onRemoveButton)
        self._ui.treeView.clicked.connect(self.onTreeItemClicked)
        self.plotWidget.scene().sigMouseMoved.connect(self.plotMouseMoved)

    def set_log_signal(self, signal):
        self.log_signal = signal
        self.backend.set_log_signal(signal)

    def setSavePathAndName(self, path, filename):
        """
        Set the save path, gets a signal from mainwindow when the save path has changed.
        """
        self.debug('setSavePathAndName: "%s", "%s"', path, filename)
        self.sardana_save_path = path
        self.sardana_file = filename
        self.onSaveNameChanged()

    def setSavePath(self, path):
        """
        Set the save path, gets a signal from mainwindow when the save path has changed.
        """
        self.sardana_save_path = path
        self.onSaveNameChanged()

    def on_file_watchdog_event(self):
        """
        Something has happened with the file we are watching, try to reolad it!
        """
        self.debug('on_file_watchdog_event')
        if not self.sardana_save_path:
            return
        try:
            filepath = os.path.join(str(self.sardana_save_path), str(self.sardana_file))
            self.backend.loadFile(filepath)
        except Exception as er:
            print("Error auto loading file: "+str(er))

    def autoLoadChanged(self, state):
        """
        Change between auto and manual load
        """
        if state == QtCore.Qt.Checked:
            self.backend.autoLoad = True
        elif state == QtCore.Qt.Unchecked:
            self.backend.autoLoad = False

    def addCrosshair(self):
        """
        Adds two lines to plotwidget that behaves as crosshair
        """
        self.crosshairvLine = pg.InfiniteLine(angle=90, movable=False)
        self.crosshairhLine = pg.InfiniteLine(angle=0, movable=False)
        self.plotWidget.addItem(self.crosshairvLine, ignoreBounds=True)
        self.plotWidget.addItem(self.crosshairhLine, ignoreBounds=True)
        QtGui.QGraphicsItem.setZValue(self.crosshairvLine, 5)  # Place in front
        QtGui.QGraphicsItem.setZValue(self.crosshairhLine, 5)

    def plotMouseMoved(self, evt):
        """
        This method moves the crosshair to the cursor
        """
        pos = evt
        if self.plotWidget.sceneBoundingRect().contains(pos):
            vb = self.plotWidget.plotItem.vb
            mousePoint = vb.mapSceneToView(pos)
            self._ui.label_coord.setText("<span style='color: green'>x=%f</span>," % mousePoint.x() +
                                         "   <span style='color: red'>y=%f</span>" % mousePoint.y())
            self.crosshairvLine.setPos(mousePoint.x())
            self.crosshairhLine.setPos(mousePoint.y())

    def addScanToPlot(self, scan):
        """
        Add a scan to plot widget. If the scan is a fileitem, it will add all the child plotitems.
        This should be the only interface to pyqtgraph adding scans
        """
        if not scan.isDisplayed:
            scan.setCheckState(QtCore.Qt.Checked)
            for item in scan.plotItems:
                self.plotWidget.removeItem(item)  # Remove first to make sure it is not double added
                self.plotWidget.addItem(item)
            scan.setIsDisplayed(True)
            scan.updatePlot()

    def removeScanFromPlot(self, scan):
        """
        Remove scan from plotwidget
        This should be the only interface to pyqtgraph removing scans
        """
        if scan.isDisplayed:
            scan.setCheckState(QtCore.Qt.Unchecked)
            for item in scan.plotItems:
                self.plotWidget.removeItem(item)
            scan.setIsDisplayed(False)

    def onNewScanLoaded(self, newScan):
        """
        This is called with a qt signal from backend when a new scan is loaded.
        It takes the x and y column settings from the last scan and applies to new scan.
        Then it adds scan to plot.
        """
        try:
            newScan.set_log_signal(self.log_signal)
        except AttributeError as e:
            print("Not able to set log signal: " +str(e))

        lastScan = self.backend.getScan(-2)
        if lastScan is not None:
            if isinstance(lastScan, scanitem.ScanItem):
                if isinstance(newScan, scanitem.ScanItem):
                    newScan.setXcolumnIndex(lastScan.xColumnIndex)
                    newScan.setYcolumnIndex(lastScan.yColumnIndex)
                    newScan.setImgcolumnIndex(lastScan.imgColumnIndex)
        if self.backend.autoLoad:
            self.addScanToPlot(newScan)

    def onDoorChanged(self, doorName):
        """
        When the taurus door is changed, this is called, it sets the correct door to the liveitem
        """
        self.doorName = doorName
        self.backend.liveItem.setDoorName(doorName)

    def onTreeItemClicked(self, index):
        """
        The treeview is clicked. If a plot is checked then it is added
        If a file is clicked then all the plots in file are added.
        If the colour is clicked the the color is randomized.
        """
        item = index.model().itemFromIndex(index)
        self._ui.treeView.setCurrentIndex(index)
        self._ui.removeButton.setEnabled(False)
        self._ui.duplicateButton.setEnabled(False)
        if isinstance(item, scanitem.ColorColumn):
            item.scan.randomColor()
            item = item.scan
        if isinstance(item, scanitem.CoreScanItem):
            self.setPlotSettings(item)
            if item.checkState() == QtCore.Qt.Checked:
                self.backend.selectObject(item)
            elif item.checkState() == QtCore.Qt.Unchecked:
                self.hideHistogram()
                self.removeScanFromPlot(item)
            if isinstance(item, scanitem.FileItem) or \
                    isinstance(item, mathitems.MathItem) or \
                    isinstance(item, roitool.RoiItem) or \
                    isinstance(item, scanitem.ScanDuplicate):
                self._ui.removeButton.setEnabled(True)
            if isinstance(item, scanitem.ScanItem) and\
                    not isinstance(item, liveitem.LiveItem):
                self._ui.duplicateButton.setEnabled(True)

    def onObjectSelected(self):
        """
        This is called from backend when an object is selected.
        """
        obj = self.backend.selectedObject
        if isinstance(obj, scanitem.CoreScanItem):
            self.addScanToPlot(obj)
            self.setPlotSettings(obj)
            if isinstance(obj, scanitem.ScanItem):
                self.hideHistogram()
                if hasattr(obj, "imageItem"):
                    if obj.is2d:
                        self.levelControlItem.setImageItem(obj.imageItem)
                        self.levelControlItem.imageChanged(autoLevel=True, autoRange=True)
                        self.showHistogram()

    def showHistogram(self):
        """
        If a 2d plot is clicked, display the histogram
        """
        self.plotWidget.getPlotItem().layout.addItem(self.levelControlItem, 2, 2)
        self.levelControlItem.show()

    def hideHistogram(self):
        """
        If a 1d scan is clicked, hide the histogram
        """
        self.levelControlItem.hide()
        self.plotWidget.getPlotItem().layout.removeItem(self.levelControlItem)

    def setPlotSettings(self, obj):
        """
        Sets the current plotsettings to the one for obj
        """
        idx = self._ui.settingsStackedWidget.indexOf(obj.settingsWidget)
        if idx == -1:
            self._ui.settingsStackedWidget.addWidget(obj.settingsWidget)
            self._ui.settingsStackedWidget.setCurrentWidget(obj.settingsWidget)
        else:
            self._ui.settingsStackedWidget.setCurrentIndex(idx)
        obj.activate()

    def onLoadRefButtonClicked(self):
        """
        Load a reference file to the list of loaded scans
        """
        oldpath = self.sardana_save_path
        file_name = QtGui.QFileDialog.getOpenFileName(None, "Load Reference File", oldpath, "Hdf5 file .h5 (*.h5)")
        newpathplusname = str(file_name[0])
        self.debug('file selected: "%s"', newpathplusname)
        if newpathplusname == "":
            return
        self.backend.loadFile(newpathplusname)

    def onReloadButtonClicked(self):
        """
        Reload the currently selected sardana savefile
        """
        self._ui.reloadButton.setEnabled(False)
        filepath = os.path.join(str(self.sardana_save_path), str(self.sardana_file))
        if not self.sardana_save_path:
            if self.log_signal is None:
                Qt.QMessageBox.critical(None, 'Path not set',
                                    "Check Experiment Config, make sure Path and File Name are set!")
            else:
                self.log_signal.emit("ERROR","Path not set! Check Experiment Config, make sure Path and File Name are set.")
        elif not os.path.exists(filepath) and not os.access(filepath, os.R_OK):
            if self.log_signal is None:
                Qt.QMessageBox.critical(None, 'Path not valid',
                                    "Check Experiment Config, make sure Path and File Name are set!")
            else:
                self.log_signal.emit("ERROR", "Path not valid! Check Experiment Config, make sure Path and File Name are set.")
        else:
            self.backend.loadFile(filepath)
        self._ui.reloadButton.setEnabled(True)

    def onUnloadButton(self):
        """
        Clear all loaded data
        """
        msg = "Are you sure you want to unload all data?"
        reply = QtGui.QMessageBox.question(None, 'Unload everything?', msg,
                                           QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            self.plotWidget.plotItem.clear()
            self.addCrosshair()
            self.backend.clear()

    def onAddSpecialButton(self):
        """
        Create popup and let the user add a mathitem
        """
        newItem = AddSpecialDialog.newItem(self.backend)
        if newItem:
            self.backend.addItem(newItem)
            self.backend.selectObject(newItem)

    def onDuplicateButton(self):
        """
        Create a duplicate of the currently selected scan
        """
        index = self._ui.treeView.currentIndex()
        if index.row() > -1:
            item = self.backend.scanModel.itemFromIndex(index)
            newItem = scanitem.ScanDuplicate(item)
            if newItem:
                self.backend.addItem(newItem)
                self.backend.selectObject(newItem)

    def onRemoveButton(self):
        """
        Remove the currently selected scan from the list of loaded scans, also unload it
        """
        index = self._ui.treeView.currentIndex()
        if index.row() > -1:
            item = self.backend.scanModel.itemFromIndex(index)
            self.removeScanFromPlot(item)
            self.backend.removeItem(item)

    def onExpConfChanged(self, expconf):
        """
        When the exprimentalconfig is changed, the sardana savefile could be edited
        """
        self.debug('onExpConfChanged')
        try:
            self.setSavePathAndName(expconf['ScanDir'], expconf["ScanFile"][0])
            #savename = expconf["ScanFile"][0]
        except Exception as er:
            if self.log_signal is None:
                Qt.QMessageBox.critical(None, 'Unable to read storagefile name',
                                    "Check Experiment Config, make sure Path and File Name are set!")
            else:
                self.log_signal.emit("ERROR","Unable to read storagefile name! Check Experiment Config, make sure Path and File Name are set.")


    def onSaveNameChanged(self):
        """
        If the savename is changed, either by applying or in sardana, the file is loaded
        """
        self.debug('onSaveNameChanged, "%s", "%s"', self.sardana_save_path, self.sardana_file)
        if not self.sardana_file:
            return
        if not self.sardana_save_path or not os.path.isdir(self.sardana_save_path):
            return
        #try:
        filepath = os.path.join(str(self.sardana_save_path), str(self.sardana_file))
        self.backend.loadFile(filepath)
        #except Exception as er:
        #    Qt.QMessageBox.critical(None, 'Exception', str(er))
        if hasattr(self, 'observer'):
            self.debug('Stopping current observer')
            if self.observer.is_alive():
                self.observer.stop()
                self.observer.join()
            else:
                self.debug('Observer was not running')
        else:
            self.debug('No observer was defined yet')
        self.observer = PollingObserver()
        self.observer.schedule(self.file_event_handler, str(self.sardana_save_path), recursive=False)
        self.observer.start()
        self.debug('New observer started')

    def onMacroStatusUpdated(self, data):
        """
        This is called from qdoor when status updated
        """

        try:
            macro = data[0]
        except (IndexError, TypeError):
            macro = None
        if macro is None:
            return

        try:
            data = data[1][0]
        except (IndexError, TypeError):
            return
        state, scanrange, step, scanid = data["state"], data["range"], data["step"], data["id"]
        self.debug('onMacroStatusUpdated, "%s", "%s", "%s", "%s"', state, scanrange, step, scanid)
        if state == "start":
            self.onMacroStart()
        elif state == "finish":
            self.onMacroFinish()
        elif state == "stop":
            self.onMacroFinish()
        else:
            self.onMacroFinish()

    def onMacroStart(self):
        """
        When macro is started, disable unload button
        """
        self._ui.unloadButton.setEnabled(False)

    def onMacroFinish(self):
        """
        When macro finishes, enable unload button
        """
        self._ui.unloadButton.setEnabled(True)

    def onRecordDataUpdated(self, *args):
        """
        Here comes the data for this step, send it to the liveitem
        """
        self.backend.liveItem.onRecordDataUpdated(args)

    def contextMenuEvent(self, event):
        """
        This is to make sure that taurusgui doesn't steal the right-click
        """
        pass

    def closeEvent(self, event):
        """
        When the widget is closed, clear the backend.
        This will i.e. close roi item popups
        """
        self.backend.clear()

    def saveSettingsToStr(self):
        """
        Taurusgui saves this string when closing.
        Here we save the plotsettings for which idxs where used.
        It is saved for the liveitem and for the latest scan.
        """
        liveitemx = 0
        liveitemy = 0
        lastscanx = 0
        lastscany = 0
        lastscanimg = 0
        if hasattr(self.backend, "liveItem"):
            liveitemx = self.backend.liveItem.xColumnIndex
            liveitemy = self.backend.liveItem.yColumnIndex
        lastScan = self.backend.getScan(-1)
        if lastScan is not None:
            lastscanx = lastScan.xColumnIndex
            lastscany = lastScan.yColumnIndex
            lastscanimg = lastScan.imgColumnIndex
        x = {"liveitemx": liveitemx,
             "liveitemy": liveitemy,
             "lastscanx": lastscanx,
             "lastscany": lastscany,
             "lastscanimg": lastscanimg}
        return json.dumps(x)

    def readSettingsFromStr(self, settingsstr):
        """
        Taurusgui gives us the string when starting.
        We apply the saved plot settings to the liveitem.
        The plotsettings for the latest scans is applied to all loaded scans.
        """
        settings = json.loads(settingsstr)
        if "liveitemx" in settings:
            if hasattr(self.backend, "liveItem"):
                self.backend.liveItem.xColumnIndex = settings["liveitemx"]
        if "liveitemy" in settings:
            if hasattr(self.backend, "liveItem"):
                self.backend.liveItem.yColumnIndex = settings["liveitemy"]
        if "lastscanx" in settings and "lastscany" in settings and "lastscanimg" in settings:
            x = settings["lastscanx"]
            y = settings["lastscany"]
            img = settings["lastscanimg"]
            self.backend.setScanSettings(x, y, img)


def main():
    app = Qt.QApplication(sys.argv)
    w = Motorscanplotter()
    w.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
