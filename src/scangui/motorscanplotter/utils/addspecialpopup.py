from ..scanitems import *
from taurus.external.qt import QtGui


class AddSpecialDialog(QtGui.QDialog):
    """
    Creates a popup dialog allowing the user to select what mathitem one wants to use.
    Displays a combobox of math operations and two comboboxes for inputscans.
    """
    def __init__(self, onedscans, twodscans):
        super(AddSpecialDialog, self).__init__(None)
        self.setWindowTitle("Add Plot Tool")
        self.resize(398, 253)
        self.verticalLayout = QtGui.QVBoxLayout(self)

        self.onedscans = onedscans
        self.twodscans = twodscans

        formlayout = QtGui.QFormLayout()
        self.layout().addLayout(formlayout)

        l1 = QtGui.QLabel("Add special plot")
        formlayout.addWidget(l1)

        l2 = QtGui.QLabel("Type")
        self.typeCombo = QtGui.QComboBox(self)
        for mathclass in available1DMathClass + available2DMathClass:
            self.typeCombo.addItem(mathclass.name, mathclass)
        formlayout.addRow(l2, self.typeCombo)
        self.typeCombo.activated.connect(self.typeComboChanged)

        l2 = QtGui.QLabel("Input1:")
        self.scan1combo = QtGui.QComboBox(self)
        for scan in onedscans:
            self.scan1combo.addItem(scan.datasetname, scan)
        formlayout.addRow(l2, self.scan1combo)

        l3 = QtGui.QLabel("Input2:")
        self.scan2combo = QtGui.QComboBox(self)
        for scan in onedscans:
            self.scan2combo.addItem(scan.datasetname, scan)
        formlayout.addRow(l3, self.scan2combo)

        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName("formLayout")

        self.CancelButton = QtGui.QPushButton(self)
        self.CancelButton.setText("Cancel")
        self.CancelButton.clicked.connect(self.reject)
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.CancelButton)

        self.AddButton = QtGui.QPushButton(self)
        self.AddButton.setText("Add")
        self.AddButton.clicked.connect(self.accept)
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.AddButton)
        self.verticalLayout.addLayout(self.formLayout)

    def typeComboChanged(self):
        """
        If type is changed to a 2D plot then only show 2d plots in the scancombos and only in one of them
        """
        idx = self.typeCombo.currentIndex()
        currentclass = self.typeCombo.itemData(idx)
        if currentclass in available2DMathClass:
            self.scan2combo.setEnabled(False)
            self.scan2combo.clear()
            self.scan1combo.clear()
            for scan in self.twodscans:
                self.scan1combo.addItem(scan.datasetname, scan)
        else:
            self.scan2combo.setEnabled(True)
            self.scan1combo.clear()
            self.scan2combo.clear()
            for scan in self.onedscans:
                self.scan1combo.addItem(scan.datasetname, scan)
                self.scan2combo.addItem(scan.datasetname, scan)

    @staticmethod
    def newItem(scanplt_backend):
        """
        Gets the avaialbe 1d and 2d scans from backend.
        Returns a new scanitem of the mathitem selected type.
        """
        all1dscans = scanplt_backend.scanlistfilter1d(scanplt_backend.loadedScans + scanplt_backend.specialItems)
        displayed1dscans = scanplt_backend.scanlistfilterDisplayed(all1dscans)
        all2dscans = scanplt_backend.scanlistfilter2d(scanplt_backend.loadedScans)
        displayed2dscans = scanplt_backend.scanlistfilterDisplayed(all2dscans)

        dialog = AddSpecialDialog(displayed1dscans, displayed2dscans)
        result = dialog.exec_()

        if result == QtGui.QDialog.Accepted:
            idx = dialog.typeCombo.currentIndex()
            newItemCls = dialog.typeCombo.itemData(idx)
            if newItemCls in available2DMathClass:
                scan1 = dialog.scan1combo.itemData(dialog.scan1combo.currentIndex())
                scanplt_backend.selectObject(scan1)
                newitm = newItemCls(scan1)
            else:
                scan1 = dialog.scan1combo.itemData(dialog.scan1combo.currentIndex())
                scan2 = dialog.scan1combo.itemData(dialog.scan2combo.currentIndex())
                scanplt_backend.selectObject(scan1)
                scanplt_backend.selectObject(scan2)
                newitm = newItemCls(scan1, scan2)
            return newitm
