from taurus.external.qt import Qt, QtCore
from watchdog.events import FileSystemEventHandler


class QtFileEventHandler(FileSystemEventHandler, Qt.QObject):
    """
    Qt signal wrapper for the watchdog that checks a directory for changes.
    Polling check is used in this project as it works for nfs mounts.
    """
    file_moved = QtCore.pyqtSignal()
    file_created = QtCore.pyqtSignal()
    file_deleted = QtCore.pyqtSignal()
    file_modified = QtCore.pyqtSignal()

    def on_moved(self, event):
        FileSystemEventHandler.on_moved(self, event)
        self.file_moved.emit()

    def on_created(self, event):
        FileSystemEventHandler.on_created(self, event)
        self.file_created.emit()

    def on_deleted(self, event):
        FileSystemEventHandler.on_deleted(self, event)
        self.file_deleted.emit()

    def on_modified(self, event):
        FileSystemEventHandler.on_modified(self, event)
        self.file_modified.emit()
