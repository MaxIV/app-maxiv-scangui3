import pyqtgraph as pg
from taurus.external.qt import QtCore, QtGui


class EmptyErrorBarItem(pg.GraphicsObject):
    """
    An error bar item that can be empty. Based on pg.ErrorBarItem but with simple adjustments allowing
    it to be empty.
    """
    def __init__(self, **opts):
        pg.GraphicsObject.__init__(self)
        self.opts = dict(
            x=None,
            y=None,
            height=None,
            width=None,
            top=None,
            bottom=None,
            left=None,
            right=None,
            beam=None,
            pen=None
        )
        self.setOpts(**opts)

    def boundingRect(self):
        x, y = self.opts['x'], self.opts['y']
        if x is None or y is None:
            return QtCore.QRectF()
        if self.path is None:
            self.drawPath()
        return self.path.boundingRect()

    def setOpts(self, **opts):
        self.opts.update(opts)
        if self.opts['x'] is None or self.opts['y'] is None:
            self.setFlag(self.ItemHasNoContents, True)
        else:
            self.setFlag(self.ItemHasNoContents, False)
        self.path = None
        self.update()
        self.informViewBoundsChanged()

    def clear(self):
        self.setFlag(self.ItemHasNoContents, True)
        self.setOpts(x=None)
        self.setOpts(y=None)
        self.path = QtGui.QPainterPath()
        self.update()
        self.informViewBoundsChanged()

    def paint(self, p, *args):
        if self.path is None:
            self.drawPath()
        pen = self.opts['pen']
        if pen is None:
            pen = pg.getConfigOption('foreground')
        p.setPen(pg.mkPen(pen))
        if self.path is None:
            return
        p.drawPath(self.path)

    def drawPath(self):
        """
        This is changed so that it will not show lines between the beam if there is more than 200 lines.
        """
        p = QtGui.QPainterPath()

        x, y = self.opts['x'], self.opts['y']
        if x is None or y is None:
            return

        height, top, bottom = self.opts['height'], self.opts['top'], self.opts['bottom']
        width, right, left = self.opts['width'], self.opts['right'], self.opts['left']

        beam = self.opts['beam']

        if height is not None or top is not None or bottom is not None:
            # draw vertical error bars
            if height is not None:
                y1 = y - height/2.
                y2 = y + height/2.
            else:
                if bottom is None:
                    y1 = y
                else:
                    y1 = y - bottom
                if top is None:
                    y2 = y
                else:
                    y2 = y + top

            if len(x) < 200:
                for i in range(len(x)):
                    p.moveTo(x[i], y1[i])
                    p.lineTo(x[i], y2[i])

            if beam is not None and beam > 0:
                x1 = x - beam/2.
                x2 = x + beam/2.
                if height is not None or top is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y2[i])
                        p.lineTo(x2[i], y2[i])
                if height is not None or bottom is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y1[i])
                        p.lineTo(x2[i], y1[i])

        if width is not None or right is not None or left is not None:
            # draw vertical error bars
            if width is not None:
                x1 = x - width/2.
                x2 = x + width/2.
            else:
                if left is None:
                    x1 = x
                else:
                    x1 = x - left
                if right is None:
                    x2 = x
                else:
                    x2 = x + right

            if len(x) < 200:
                for i in range(len(x)):
                    p.moveTo(x1[i], y[i])
                    p.lineTo(x2[i], y[i])

            if beam is not None and beam > 0:
                y1 = y - beam/2.
                y2 = y + beam/2.
                if width is not None or right is not None:
                    for i in range(len(x)):
                        p.moveTo(x2[i], y1[i])
                        p.lineTo(x2[i], y2[i])
                if width is not None or left is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y1[i])
                        p.lineTo(x1[i], y2[i])

        self.path = p
        self.prepareGeometryChange()
