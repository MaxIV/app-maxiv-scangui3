import os

import h5py

from .scanitems import scanitem
from .dataset import loadTitle, loadDataset
import logging

logger = logging.getLogger(__name__)

def getNewScans(fileitem, auto_load=False):
    """
    Generator that loads the dataset file if exists, if loaded, refresh, yields new scans

    yields scan
    """
    if os.path.exists(fileitem.filename) and os.access(fileitem.filename, os.R_OK):
        logger.debug('Loading file: "%s"', fileitem.filename)
        try:
            with h5py.File(fileitem.filename, 'r', locking=False) as f:
                datasets = list(f.keys())
            for dataset in datasets:
                scan = fileitem.getScanItem(dataset)
                if scan is None:
                    # Scan was not loaded before, load it
                    scan = scanitem.ScanItem(fileitem.filename, dataset)
                    fileitem.appendRow(scan)
                    loadTitle(scan)
                    if auto_load:
                        loadDataset(scan)
                    yield scan
                else:
                    if not scan.isComplete:
                        loadTitle(scan)
                        if auto_load:
                            loadDataset(scan)
                            scan.ignoreNextAxisSelectionCheck()
                            scan.updatePlot()
        except IOError as e:
            logger.debug('Unable to read h5 file: "%s"', fileitem.filename)
            logger.debug(e)
    else:
        logger.debug('Unable to open file: "%s"', fileitem.filename)
