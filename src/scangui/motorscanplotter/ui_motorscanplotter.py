# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './scanplotter.ui'
#
# Created: Thu Oct 29 16:14:50 2015
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from taurus.external.qt import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(958, 622)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(3)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        self.gridLayout_2 = QtGui.QGridLayout(Form)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.splitter = QtGui.QSplitter(Form)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.widget = QtGui.QWidget(self.splitter)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QtCore.QSize(100, 10))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.gridLayout = QtGui.QGridLayout(self.widget)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.plotLayout = QtGui.QVBoxLayout()
        self.plotLayout.setObjectName(_fromUtf8("plotLayout"))
        self.gridLayout.addLayout(self.plotLayout, 0, 0, 1, 1)
        self.widget_2 = QtGui.QWidget(self.splitter)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy)
        self.widget_2.setObjectName(_fromUtf8("widget_2"))
        self.gridLayout_3 = QtGui.QGridLayout(self.widget_2)
        self.gridLayout_3.setSpacing(0)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setSpacing(0)
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        #self.line_savename = QtGui.QLineEdit(self.widget_2)
        #self.line_savename.setObjectName(_fromUtf8("line_savename"))
        #self.horizontalLayout_9.addWidget(self.line_savename)
        self.formLayout.setLayout(0, QtGui.QFormLayout.FieldRole, self.horizontalLayout_9)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        #self.applySaveButton = QtGui.QPushButton(self.widget_2)
        #self.applySaveButton.setEnabled(True)
        #self.applySaveButton.setObjectName(_fromUtf8("applySaveButton"))
        #self.horizontalLayout.addWidget(self.applySaveButton)
        self.reloadButton = QtGui.QPushButton(self.widget_2)
        self.reloadButton.setObjectName(_fromUtf8("reloadButton"))
        self.horizontalLayout.addWidget(self.reloadButton)
        self.formLayout.setLayout(2, QtGui.QFormLayout.FieldRole, self.horizontalLayout)
        self.loadRefButton = QtGui.QPushButton(self.widget_2)
        self.loadRefButton.setObjectName(_fromUtf8("loadRefButton"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.loadRefButton)
        self.unloadButton = QtGui.QPushButton(self.widget_2)
        self.unloadButton.setObjectName(_fromUtf8("unloadButton"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.unloadButton)
        self.autoLoad = QtGui.QCheckBox(self.widget_2)
        self.autoLoad.setObjectName("autoLoad")
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.autoLoad)
        self.verticalLayout.addLayout(self.formLayout)
        self.treeView = QtGui.QTreeView(self.widget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.treeView.sizePolicy().hasHeightForWidth())
        self.treeView.setSizePolicy(sizePolicy)
        self.treeView.setObjectName(_fromUtf8("treeView"))
        self.verticalLayout.addWidget(self.treeView)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.duplicateButton = QtGui.QPushButton(self.widget_2)
        self.duplicateButton.setEnabled(False)
        self.duplicateButton.setObjectName(_fromUtf8("duplicateButton"))
        self.horizontalLayout_2.addWidget(self.duplicateButton)
        self.removeButton = QtGui.QPushButton(self.widget_2)
        self.removeButton.setEnabled(False)
        self.removeButton.setObjectName(_fromUtf8("removeButton"))
        self.horizontalLayout_2.addWidget(self.removeButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.addSpecialButton = QtGui.QPushButton(self.widget_2)
        self.addSpecialButton.setObjectName(_fromUtf8("addSpecialButton"))
        self.verticalLayout.addWidget(self.addSpecialButton)
        self.label_coord = QtGui.QLabel(self.widget_2)
        self.label_coord.setObjectName(_fromUtf8("label_coord"))
        self.verticalLayout.addWidget(self.label_coord)
        self.settingsStackedWidget = QtGui.QStackedWidget(self.widget_2)
        self.settingsStackedWidget.setObjectName(_fromUtf8("settingsStackedWidget"))
        self.plotsettingsStackedWidgetPage1 = QtGui.QWidget()
        self.plotsettingsStackedWidgetPage1.setObjectName(_fromUtf8("plotsettingsStackedWidgetPage1"))
        self.gridLayout_6 = QtGui.QGridLayout(self.plotsettingsStackedWidgetPage1)
        self.gridLayout_6.setObjectName(_fromUtf8("gridLayout_6"))
        self.label_selectedscan = QtGui.QLabel(self.plotsettingsStackedWidgetPage1)
        self.label_selectedscan.setObjectName(_fromUtf8("label_selectedscan"))
        self.gridLayout_6.addWidget(self.label_selectedscan, 1, 0, 1, 1)
        self.settingsStackedWidget.addWidget(self.plotsettingsStackedWidgetPage1)
        self.verticalLayout.addWidget(self.settingsStackedWidget)
        self.gridLayout_3.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.splitter, 0, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Form", None))
        #self.line_savename.setText(_translate("Form", "/home/ludde/Desktop/ascangui/ascandata", None))
        #self.applySaveButton.setText(_translate("Form", "Apply", None))
        self.reloadButton.setText(_translate("Form", "Reload", None))
        self.loadRefButton.setText(_translate("Form", "Load reference...", None))
        self.unloadButton.setText(_translate("Form", "Clear", None))
        self.autoLoad.setText(_translate("Form", "Auto load scans", None))
        self.duplicateButton.setText(_translate("Form", "Duplicate", None))
        self.removeButton.setText(_translate("Form", "Remove", None))
        self.addSpecialButton.setText(_translate("Form", "Math...", None))
        self.label_coord.setText(_translate("Form", "Coordinate label", None))
        self.label_selectedscan.setText(_translate("Form", "Settings Widget", None))
