import os

import h5py
import numpy as np

import logging

logger = logging.getLogger(__name__)


def getEntry(key, nxentry):
    """Small function to get start_time / end_time / title"""
    try:
        # Old format?
        return nxentry[key][...][0]
    except KeyError:
        return ""
    except IndexError:
        return nxentry[key][()].decode("utf-8")


def loadTitle(scan):
    """
    Loads the dataset from the file specified in the scan into the scan
    """
    logger.debug(f"LoadTitle {scan}")
    with h5py.File(scan.filename, "r", locking=False) as f:
        if scan.datasetname in f:
            nxentry = f[scan.datasetname]
            start_time = getEntry("start_time", nxentry)
            end_time = getEntry("end_time", nxentry)
            if end_time:
                 scan.isComplete = True
                 logger.debug(f"{scan} is complete!")
            title = getEntry("title", nxentry)
            if title:
                scan.setTitle(f"{title}\nStart: {start_time}\nEnd: {end_time}")


def loadDataset(scan):
    """
    Loads the dataset from the file specified in the scan into the scan
    """
    logger.debug(f"LoadDataset {scan}")
    with h5py.File(scan.filename, "r", locking=False) as f:
        if scan.datasetname in f:
            nxentry = f[scan.datasetname]
            # Transitional check if the old temporary fileformat was used
            if 'header' in nxentry.attrs:
                try:
                    oldloadDataset(scan, nxentry)
                except Exception:
                    pass
                return

            instr_eiger = None
            measurement = None
            if "instrument" in nxentry:
                # Try to read Eiger instrument data
                try:
                    instr_eiger = nxentry["instrument"]["eiger"]
                    measurement = instr_eiger
                except Exception:
                    print("No Eiger instrument data found!")

            if "measurement" in nxentry:
                measurement = nxentry["measurement"]
            elif "data" in nxentry:
                measurement = nxentry["data"]

            if not measurement:
                raise Exception("Dataset is of unknown type")

            header = []
            values = []
            oned = []
            twod = []
            threed = []
            i = 0
            for group in measurement:
                if type(measurement[group]) is h5py.Dataset:
                    data = measurement[group][...]
                    shape = measurement[group].shape
                    if len(shape) == 1:
                        header.append(group)
                        oned.append(i)
                        values.append(data)
                        i += 1
                    elif len(shape) == 2:
                        if len(data):
                            header.append(group)
                            twod.append(i)
                            values.append(data)
                            i += 1
                            # Add rows and columns from 2D data to be selectable as X or Y scale
                            r0 = data[0, :]
                            if validateScaleData(r0):
                                header.append(group + "_row0")
                                oned.append(i)
                                values.append(r0)
                                i += 1
                            c0 = data[:, 0]
                            if validateScaleData(c0):
                                header.append(group + "_col0")
                                oned.append(i)
                                values.append(c0)
                                i += 1
                    elif len(shape) == 3:
                        # Rudimentary 3D support by storing hdf5 reference and later opening it when needed
                        header.append(group)
                        values.append(measurement[group].ref)
                        threed.append(i)
                        i += 1
                        # Adding the latest slice as a 2D image
                        if len(data):
                            header.append(group+"_frame")
                            values.append(data[-1, :, :])
                            twod.append(i)
                            i += 1

            # Read Eiger instrument data for sequence number and timestamp
            if instr_eiger:
                group = "start_time"
                if group in instr_eiger:
                    names = instr_eiger[group].attrs["names"]
                    data = instr_eiger[group][...]
                    data_transposed = np.transpose(data)
                    start_times = data[0]
                    for name, data, start in zip(names, data_transposed, start_times):
                        if name == "sequence_number":
                            header.append("eiger_seq_num")
                            oned.append(i)
                            values.append(data)
                            i += 1
                        elif name == "timestamp":
                            # Decode Eiger timestamp (ns) and convert to dt (s)
                            dt = [float((d-start)/1e9) for d in data]
                            header.append("eiger_dt")
                            oned.append(i)
                            values.append(dt)
                            i += 1

            scan.setHeader(header)
            scan.setColumns(values, oned, twod, threed)



def oldloadDataset(scan, dset):
    """
    Load datasets from the old temporary format
    The entrys have header and macroline attributes
    """
    header = dset.attrs['header']
    data = dset[...]
    try:
        title = str(dset.attrs["macroline"])
        scan.setTitle(title)
    except:
        pass  # Title not loaded, no worries

    if header[0] == "fulhack":
        # Fulhack are scans that never got any points, however they were saved so we have to handle them somehow
        header = list(map(str, range(0, len(data[0]))))
    scan.setHeader(header)
    scan.setColumnsFromRecords(data)


def validateScaleData(data):
    # Check that array is larger than 1 and all values are NOT equal.
    ok = False
    if data.size > 1:
        ok = not np.all(data == data[0])
    return ok
