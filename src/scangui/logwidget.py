import datetime
import functools
import json

#import tango
import numpy as np
import taurus
from taurus.core import TaurusDevState
from taurus.external.qt import Qt, QtGui
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.display import TaurusLabel
from taurus.qt.qtgui.resource import getThemeIcon
import time
import logging

class LogWidget(TaurusWidget):
    """
    Simple text window to show log output
    """
    append_log = Qt.pyqtSignal((str,str))

    def __init__(self, parent=None):
        TaurusWidget.__init__(self)
        self.initUI()
        #self.setupQtConnections()
        self.append_log.connect(self.update_log)

        #self.registerConfigProperty("saveSettingsToStr", "readSettingsFromStr", "basicexecutor")

    def initUI(self):
        self.verticalLayout = QtGui.QVBoxLayout(self)
        self.setLayout(self.verticalLayout)
        self.setMinimumSize(1, 30)

        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        self.setSizePolicy(sizePolicy)
        self.resize(10, 10)
        

        self.logbox = QtGui.QTextEdit()
        self.logbox.setReadOnly(True)
        self.verticalLayout.addWidget(self.logbox)
        self.clearButton = QtGui.QPushButton("Clear")
        self.verticalLayout.addWidget(self.clearButton)

    def update_log(self, level, text):
        plain = self.logbox.textColor()
        if level == "FATAL":
            color = QtGui.QColor(Qt.Qt.red)
        elif level == "ERROR":
            color = QtGui.QColor(Qt.Qt.red)
        elif level == "WARN":
            color = QtGui.QColor(Qt.Qt.darkYellow)
        elif level == "DEBUG":
            color = QtGui.QColor(Qt.Qt.blue) 
        elif level == "INFO":
            color = QtGui.QColor(Qt.Qt.cyan)
        else:
            color = QtGui.QColor(Qt.Qt.black)     
        self.logbox.setTextColor(color)
        #print(text)
        date_time = time.strftime("%Y-%m-%d, %H:%M:%S")
        self.logbox.append("[{}] {}".format(date_time, text))
        self.logbox.setTextColor(plain)



