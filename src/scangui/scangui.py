import datetime
import os
import sys
import time
import argparse

import sardana.taurus.qt.qtgui.extra_macroexecutor.common as macroexecutor_common
import sardana.taurus.qt.qtgui.extra_macroexecutor.macroexecutor as macroexecutor
import sardana.taurus.qt.qtgui.extra_macroexecutor.sequenceeditor.sequenceeditor as sequenceeditor
from sardana.taurus.qt.qtgui.extra_sardana import ExpDescriptionEditor
from taurus.external.qt import Qt, QtGui, compat
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.base import TaurusBaseComponent
from taurus.qt.qtgui.resource import getThemeIcon, getIcon
from taurus.qt.qtgui.taurusgui import TaurusGui
from taurus import Attribute
from taurus.core.tango import DevState
from taurus.core import TaurusDevState
from taurus.core.taurusbasetypes import TaurusEventType
from taurus.core.taurusexception import TaurusException
import taurus.core.util.argparse
import taurus
import pickle
#import tango
import logging

from scangui.basicexecutor import BasicExecutorWidget
from scangui.motorscanplotter.motorscanplotter import Motorscanplotter
from scangui.movables import MovableConfiguration
from scangui.logwidget import LogWidget

GUI_NAME = "Scangui"
ORGANIZATION = "MAXIV"
CUSTOM_LOGO = "images/MAXlogo.jpg"
MANUAL_URI = "http://wiki.maxiv.lu.se/index.php/SPECIES_Scangui"

class ScanguiSplashTaurusGui(TaurusGui):
    SPLASH_LOGO_NAME = os.path.join(os.path.dirname(__file__),"images/Scanguisplash.jpg")

    experimentConfigurationChanged = Qt.pyqtSignal(compat.PY_OBJECT)
    sardanaSavePath = Qt.pyqtSignal('QString')

    def main(self):
        self.movable_filters_filename = None
        self._doorname = None
        self._macroservername = None
        self._sardana_save_path = None #NAH, this should be the same as the button we dont know; "_ui.fjkljfdslk"?

        # Internal connections
        Qt.qApp.SDM.connectReader("doorName", self.onDoorChanged)
        Qt.qApp.SDM.connectReader("macroserverName", self.onMacroServerChanged)
        Qt.qApp.SDM.connectReader("clientSardanaSavePathChanged", self.onSavePathChanged)
        Qt.qApp.SDM.connectWriter("doorName", self, 'doorNameChanged')
        Qt.qApp.SDM.connectWriter("macroserverName", self, 'macroserverNameChanged')
        Qt.qApp.SDM.connectWriter("clientSardanaSavePathChanged", self, 'sardanaSavePath')

        self.registerConfigProperty(self.saveSettingsToStr, self.readSettingsFromStr, "Scangui_init")

        # Add basic executor
        self.showSplashMessage("Adding basic executor")
        self.basicexecutor = BasicExecutorWidget(self)

        # Instantiate the configurator for filtering movables.
        self.basicexecutor.movable_configuration = MovableConfiguration()

        # Load the movables configuration file from a known location.
        if self.movable_filters_filename is not None:
            try:
                self.basicexecutor.movable_configuration.configure_from_file(
                    self.movable_filters_filename)
            except Exception as exception:
                print('%s: error: cannot load %s: %s' % (os.path.basename(sys.argv[0]), self.movable_filters_filename, str(exception)))
                sys.exit(-1)
        
        self.createPanel(self.basicexecutor, "Basic executor", registerconfig=True, permanent=True)
        Qt.qApp.SDM.connectReader("macroStatus", self.basicexecutor.onMacroStatusUpdated)
        Qt.qApp.SDM.connectReader("doorName", self.basicexecutor.onDoorChanged)
        Qt.qApp.SDM.connectReader("macroserverName", self.basicexecutor.onMacroserverChanged)
        Qt.qApp.SDM.connectReader("doorErrorChanged", self.basicexecutor.onDoorError)
        Qt.qApp.SDM.connectReader("recordDataUpdated", self.basicexecutor.onRecordDataUpdated)
        Qt.qApp.SDM.connectWriter("doorName", self.basicexecutor, 'doorNameChanged')
        Qt.qApp.SDM.connectWriter("shortMessage", self.basicexecutor, "shortMessageEmitted")

        # Add macroexecutor
        self.showSplashMessage("Adding macroexecutor")
        self.macroExecutor = macroexecutor.TaurusMacroExecutorWidget()
        self.createPanel(self.macroExecutor, 'MacroExecutor', registerconfig=True, permanent=True)
        Qt.qApp.SDM.connectReader("macroserverName", self.macroExecutor.setModel)
        Qt.qApp.SDM.connectReader("doorName", self.macroExecutor.onDoorChanged)
        Qt.qApp.SDM.connectReader("macroStatus", self.macroExecutor.onMacroStatusUpdated)
        Qt.qApp.SDM.connectWriter("macroName", self.macroExecutor, "macroNameChanged")
        Qt.qApp.SDM.connectWriter("executionStarted", self.macroExecutor, "macroStarted")
        Qt.qApp.SDM.connectWriter("shortMessage", self.macroExecutor, "shortMessageEmitted")

        # Add sequencer
        self.showSplashMessage("Adding sequencer")
        self.sequenceeditor = sequenceeditor.TaurusSequencerWidget()
        self.createPanel(self.sequenceeditor, 'Sequencer', registerconfig=True, permanent=True)
        Qt.qApp.SDM.connectReader("macroserverName", self.sequenceeditor.setModel)
        Qt.qApp.SDM.connectReader("doorName", self.sequenceeditor.onDoorChanged)
        Qt.qApp.SDM.connectReader("macroStatus", self.sequenceeditor.onMacroStatusUpdated)
        Qt.qApp.SDM.connectWriter("macroName", self.sequenceeditor.tree, "macroNameChanged")
        Qt.qApp.SDM.connectWriter("macroName", self.sequenceeditor, "macroNameChanged")
        Qt.qApp.SDM.connectWriter("executionStarted", self.sequenceeditor, "macroStarted")
        Qt.qApp.SDM.connectWriter("shortMessage", self.sequenceeditor, "shortMessageEmitted")

        # Add expconf
        self.showSplashMessage("Adding experiemental config")
        self.expconf = ExpDescriptionEditor()
        Qt.qApp.SDM.connectReader("doorName", self.expconf.setModel)
        self.createPanel(self.expconf,
                        'Experiment Config',
                        registerconfig=True,
                        icon=getThemeIcon('preferences-system'),
                        permanent=True)
        ###############################
        # TODO: These lines can be removed once the door does emit
        # "experimentConfigurationChanged" signals
        Qt.qApp.SDM.connectWriter("expConfChanged", self.expconf,
                                  "experimentConfigurationChanged")
        ###############################

        # Create macroconfiguration dialog & action
        self.__macroConfigurationDialog = macroexecutor_common.TaurusMacroConfigurationDialog(self)
        self.macroConfigurationAction = self.taurusMenu.addAction(getThemeIcon("preferences-system-session"),
                                                                 "Macro execution configuration...",
                                                                 self.__macroConfigurationDialog.show)
        Qt.qApp.SDM.connectReader("macroserverName", self.__macroConfigurationDialog.selectMacroServer)
        Qt.qApp.SDM.connectReader("doorName", self.__macroConfigurationDialog.selectDoor)
        Qt.qApp.SDM.connectWriter("macroserverName", self.__macroConfigurationDialog, 'macroserverNameChanged')
        Qt.qApp.SDM.connectWriter("doorName", self.__macroConfigurationDialog, 'doorNameChanged')

        self.showSplashMessage("Adding scanplotter")
        self.scanplotter = Motorscanplotter()
        self.createPanel(self.scanplotter, "Scanplotter", registerconfig=True, permanent=True)
        Qt.qApp.SDM.connectWriter("expConfChanged", self.scanplotter, "experimentConfigurationChanged")
        Qt.qApp.SDM.connectReader("macroStatus", self.scanplotter.onMacroStatusUpdated)
        Qt.qApp.SDM.connectReader("doorName", self.scanplotter.onDoorChanged)
        Qt.qApp.SDM.connectReader("expConfChanged", self.scanplotter.onExpConfChanged)
        Qt.qApp.SDM.connectReader("recordDataUpdated", self.scanplotter.onRecordDataUpdated)
        Qt.qApp.SDM.connectReader("clientSardanaSavePathChanged", self.scanplotter.setSavePath)

        # Log window
        self.showSplashMessage("Adding log window")
        self.logviewer = LogWidget()
        self.createPanel(self.logviewer, "Log", registerconfig=True, permanent=True)
        self.logviewer.append_log.emit("INFO", "Info")
        self.logviewer.append_log.emit("WARN", "Warning")
        self.logviewer.append_log.emit("DEBUG", "Debug")
        self.logviewer.append_log.emit("ERROR", "Error")
        self.logviewer.append_log.emit(None, "Just text")
        self.scanplotter.set_log_signal(self.logviewer.append_log)
        self.basicexecutor.set_log_signal(self.logviewer.append_log)

        # Hide unused bar
        self.jorgsBar.hide()
        self.perspectivesToolBar.hide()
        self.panelsToolBar.hide()
        #self.getPanel('Manual').hide()

        self.showSplashMessage("Loading settings")
        defaultini = os.path.join(self._confDirectory, "default.ini")
        self.loadSettings(settings=None, factorySettingsFileName=defaultini)
        
        # Add to right click
        #self.showSplashMessage("Checking save path")
        #if self._sardana_save_path is None or self._sardana_save_path == "" or self._sardana_save_path == "None":
        #    self.getSavePath()
        
        door = self.getModelObj()

        self.showSplashMessage("Looking for door")

        if door is None:
            self.debug('No Door defined at startup')
            self.__macroConfigurationDialog.show()
        else:
            try:
                doorState = Attribute(self.getModelName(), "State").read().rvalue
            except Exception as e:
                self.debug('Unable to read Door state, "%s"', self.getModelName())
                doorState = None
            if doorState in (DevState.RUNNING, DevState.STANDBY, DevState.ON, DevState.ALARM, TaurusDevState.Ready):
                expconf = door.getExperimentConfiguration()
                if "ScanDir" in door.macro_server.getEnvironment():
                    expconf['ScanDir'] = door.macro_server.getEnvironment("ScanDir")
                    self.scanplotter.setSavePath(door.macro_server.getEnvironment("ScanDir"))
                    self._sardana_save_path = door.macro_server.getEnvironment("ScanDir")
                    self.debug('Set ScanDir: "%s"', expconf['ScanDir'])
                else:
                    self.debug('ScanDir not defined in expconf')
                    expconf['ScanDir'] = None
                    self.scanplotter.setSavePath(None)
                    self._sardana_save_path = None
                door.experimentConfigurationChanged.emit(expconf)

        # Add panic button for aborting the door
        text = "Panic Button: stops the pool (double-click for abort)"
        self.doorAbortAction = self.quickAccessToolBar.addAction(
                                Qt.QIcon("actions:process-stop.svg"),
                                text, self.__onDoorAbort)

        # store beginning of times as a datetime
        self.__lastAbortTime = datetime.datetime(1, 1, 1)

        # store doubleclick interval as a timedelta
        td = datetime.timedelta(0, 0, 1000 * Qt.qApp.doubleClickInterval())
        self.__doubleclickInterval = td

        #This created a loop of expConfChanged, probably not needed any more anyway
        #Qt.qApp.SDM.connectReader("expConfChanged", self.expConfigChanged)

        # Hide the history and favorites due to bug were they are unusable http://sourceforge.net/p/sardana/tickets/17/
        self.macroExecutor.tabMacroListsWidget.hide()

    def createHelpMenu(self):
        """
        Override to remove Manual option in Help menu
        """
        self.helpMenu = self.menuBar().addMenu("Help")
        self.helpMenu.addAction("About ...", self.showHelpAbout)
    
    def showSplashMessage(self, msg):
        """
        Show some information on the splash screen
        """
        try: self.splashScreen().showMessage(msg)
        except AttributeError: pass

    #def expConfigChanged(self, expconf):
    #    """
    #    Hack to make the expconf update if changed from scanplotter
    #    """
    #    self.debug('expConfigChanged')
    #    self.expconf.setModel(self._doorname)  # Force refresh

    def onMacroServerChanged(self, model):
        """
        Macroserver has changed, store and then save when exiting
        """
        self.debug('onMacroServerChanged, "%s"', model)
        self._macroservername = str(model)

    def onSavePathChanged(self, path):
        self.debug('onSavePathChanged, "%s"', path)
        self._sardana_save_path = str(path)

    def onDoorChanged(self, model):
        """
        Door has changed, set model!
        """
        self.debug('onDoorChanged, "%s"', model)
        try:
            doorState = Attribute(model, "State").read().rvalue
        except Exception as e:
            self.debug('Unable to read Door state')
            doorState = "(not responding)"
        if doorState in (DevState.RUNNING, DevState.STANDBY, DevState.ON, DevState.ALARM, TaurusDevState.Ready):
            self.setModel(model)
            door = self.getModelObj()
            expconf = door.getExperimentConfiguration()
            if "ScanDir" in door.macro_server.getEnvironment():
                scan_dir_path = door.macro_server.getEnvironment("ScanDir")
            else:
                scan_dir_path = None
            expconf['ScanDir'] = scan_dir_path
            self.scanplotter.setSavePath(scan_dir_path)
            self._sardana_save_path = scan_dir_path
            door.experimentConfigurationChanged.emit(expconf)
        else:
            self.debug('Door is not in a usable state')
            msgBox = QtGui.QMessageBox()
            msgBox.setWindowTitle("{} not responding".format(model))
            msgBox.setText("The selected Door doesn't respond.\n"
                           "The door state is: "+str(doorState)+"\n"
                           "Select another Door or try restarting the MacroServer.")
            msgBox.exec_()


    def setModel(self, doorname):
        """ Slot to be called when the door has changed. It updates connections of the door.

        :param doorname: (str) the tango name of the door device
        """
        self.debug('setModel "%s"', doorname)
        dataUIDs = ["macroStatus", "doorOutputChanged", "doorInfoChanged", "doorWarningChanged", "doorErrorChanged",
                    "doorDebugChanged", "doorResultChanged", "expConfChanged", "recordDataUpdated"]

        doorsignals = ["macroStatusUpdated", "outputUpdated", "infoUpdated", "warningUpdated", "errorUpdated",
                       "debugUpdated", "resultUpdated", "experimentConfigurationChanged", "recordDataUpdated"]
        if doorname is None:
            return
        door = self.getModelObj()
        if door is not None:  # disconnect it from *all* shared data providing
              for dataUID, signalname in zip(dataUIDs, doorsignals):
                if hasattr(door, signalname):  # check that signals are connected before trying to disconnect
                    Qt.qApp.SDM.disconnectWriter(dataUID, door, signalname)

        TaurusBaseComponent.setModel(self, doorname)
        self._doorname = doorname
        if doorname == "":
            return
        door = self.getModelObj()
        if not isinstance(door, Qt.QObject):
            msg = "cannot connect to door %s" % doorname
            Qt.QMessageBox.critical(None, 'Door connection error', msg)
            return
        self.doorname = doorname

        if door:  # Reconnect the signals to the new door
            for dataUID, signalname in zip(dataUIDs, doorsignals):
                Qt.qApp.SDM.connectWriter(dataUID, door, signalname)

        # Somehow this doesn't automatically get fetched
        expconf = door.getExperimentConfiguration()
        door.experimentConfigurationChanged.emit(expconf)
        # check if JsonRecorder env var is set
        if 'JsonRecorder' not in door.getEnvironment():
            msg = "JsonRecorder environment variable is not set, but it is needed for displaying trend plots. \n" +\
                  "Enable it globally for %s?" % doorname
            result = Qt.QMessageBox.question(None, 'JsonRecorder not set', msg, Qt.QMessageBox.Yes | Qt.QMessageBox.No)
            if result == Qt.QMessageBox.Yes:
                door.putEnvironment('JsonRecorder', True)
                print("Json Enabled")

    def saveSettingsToStr(self):
        """
        Taurusgui saves this string when closing
        """
        x = ",".join(
            [
                f"sardanasavepath:{self._sardana_save_path}",
                f"macroserver:{self._macroservername}",
                f"doorname:{self._doorname}",
                f"autoload:{self.scanplotter.backend.autoLoad}",
            ]
        )
        return x

    def readSettingsFromStr(self, settingsstr):
        """
        Taurusgui gives us the string when starting
        """
        sd = dict(u.split(":") for u in settingsstr.split(","))
        for key, value in list(sd.items()):
            if key == "macroserver":
                if not value == "" and not value == "None":
                    try:
                        #ds = tango.DeviceProxy(value)
                        ds = taurus.Device(value)
                        msState = ds.read_attribute("State").value
                    except Exception as e:
                        msState = None
                    if msState in (DevState.RUNNING, DevState.STANDBY, DevState.ON, TaurusDevState.Ready):
                        self.macroserverNameChanged.emit(value)
            if key == "doorname":
                if not value == "" and not value == "None":
                    try:
                        #ds = tango.DeviceProxy(value)
                        ds = taurus.Device(value)
                        doorState = ds.read_attribute("State").value
                    except Exception as e:
                        doorState = "(not responding)"
                    if doorState in (DevState.RUNNING, DevState.STANDBY, DevState.ON, DevState.ALARM, TaurusDevState.Ready):
                        self.doorNameChanged.emit(value)
                    else:
                        msgBox = QtGui.QMessageBox()
                        msgBox.setWindowTitle("{} not responding".format(value))
                        msgBox.setText("The selected Door doesn't respond and Scangui cannot continue.\n"
                                       "The door state is: "+str(doorState)+"\n"
                                       "Try restarting the MacroServer.")
                        msgBox.setInformativeText("Do you wish to select another door?")
                        msgBox.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
                        msgBox.setDefaultButton(QtGui.QMessageBox.No)
                        retval = msgBox.exec_()
                        if retval == QtGui.QMessageBox.Yes:
                            self._macroservername = None
                            self._doorname = None
                        else:
                            sys.exit()
            if key == "sardanasavepath":
                if value == "" or value == "None":
                    value = None
                self.sardanaSavePath.emit(value)
            if key == "autoload":
                if value == "True":
                    self.scanplotter._ui.autoLoad.setChecked(True)

    def __onDoorAbort(self):
        """
        Copied from macrobroker
        slot to be called when the abort action is triggered.
        It sends stop command to the pools (or abort if the action
        has been triggered twice in less than self.__doubleclickInterval

        .. note:: An abort command is always preceded by an stop command
        """
        # decide whether to send stop or abort
        now = datetime.datetime.now()
        if now - self.__lastAbortTime < self.__doubleclickInterval:
            doorCmd = "AbortMacro"
            poolCmd = "Abort"
        else:
            doorCmd = "StopMacro"
            poolCmd = "Stop"

        # send stop/abort to door
        door = self.getModelObj()
        door.command_inout(doorCmd)
        self.info('Sending %s command to %s' % (doorCmd, self._doorname))
        self.newShortMessage.emit("%s command sent to door" % doorCmd)

        # send stop/abort to all pools
        pools = door.macro_server.getElementsOfType('Pool')
        for pool in list(pools.values()):
            self.info('Sending %s command to %s' % (poolCmd, pool.getFullName()))
            try:
                pool.getObj().command_inout(poolCmd)
            except:
                self.info('%s command failed on %s', poolCmd, pool.getFullName(),
                          exc_info=1)
        self.newShortMessage.emit("%s command sent to all pools" % poolCmd)
        self.__lastAbortTime = now

class Scangui(Qt.QObject, TaurusBaseComponent):
    """
    The species scangui has basically the same structure as the taurusgui macropanel.
    This class contains the shared data manager connections. with it the doors qt signals are connceted
    to the different tauruspanels.
    """
    def __init__(self, parent=None):
        Qt.QObject.__init__(self, parent)
        TaurusBaseComponent.__init__(self, self.__class__.__name__)
        self.movable_filters_filename = None

    def main(self):
        self.setLogLevel(logging.DEBUG)
        self.debug('Starting Scangui')

        parser = self.addExtraArguments()
        app = TaurusApplication(sys.argv, cmd_line_parser=parser)
        self.validateExtraArguments(app)

        self.gui = ScanguiSplashTaurusGui(confname=__file__)

        self.setLogLevel(logging.DEBUG)
        
        self.gui.main()

        self.gui.show()
        sys.exit(app.exec_())

    def addExtraArguments(self):
        """
        Add command line argumentss.
        """
        # Where we should look for the motor filters file argument.
        self.movable_filters_directory = "~/.config/%s" % (ORGANIZATION)

        # Make Taurus parser.
        parser = taurus.core.util.argparse.get_taurus_parser()

        # Add the extra option for specifying the movable filters.
        parser.add_option('--movable_filters_filename', 
            help=('The name of the JSON file containing the movable filters. ' +
            'This file should be located in %s. ' +
            'The default is no filtering.') % (self.movable_filters_directory))

        return parser

    def validateExtraArguments(self, app):
        """
        Validate command line argumentss.
        """
        args = app.get_command_line_options()

        # Make sure the file exists.
        if args.movable_filters_filename is not None:
            self.movable_filters_filename = os.path.expanduser(
                self.movable_filters_directory + "/" + args.movable_filters_filename)
            if not os.path.isfile(self.movable_filters_filename):
                print("%s: error: the motor filters file does not exist: %s" % (os.path.basename(sys.argv[0]), self.movable_filters_filename))
                sys.exit(-1)


def main():
    scangui = Scangui()
    scangui.main()

if __name__ == "__main__":
    main()
