import os
import json
import re
import logging

class MovableConfiguration(object):
    """
    This class configures the list of movables that show on the gui.
    """
    # -----------------------------------------------------------------
    def __init__(self, parent=None):
        self.parent = parent
        self.include_regexps = None


    # -----------------------------------------------------------------
    def configure_from_file(self, filename):
        '''
        Given a filename, get the configuration.
        Throw exception if file does not exist or cannot be read, or has invalid json or regexp patterns.
        '''

        # Start with null inclusions (means accept all).
        self.include_regexps = None

        jsondict = None
        with open(filename) as jsonfile:  
            jsondict = json.load(jsonfile)
            self.debug("loaded movables configuration file %s" % (filename))

        self.configure_from_jsondict(jsondict)


    # -----------------------------------------------------------------
    def configure_from_jsondict(self, jsondict):
        '''
        Given a dict read from json, get the configuration.
        No error if the dict is empty, None or has invalid regexp patterns.
        '''

        # Start with null inclusions (means accept all).
        self.include_regexps = None
        
        if jsondict is None:
            return

        # Reference the list of inclusion patterns from within the json.
        include_patterns = jsondict.get("include")

        if include_patterns is not None:
            self.include_regexps = []

            # Compile each pattern in the list into a regexp.
            parsing_pattern = ""
            try:
                for pattern in include_patterns:
                    parsing_pattern = pattern
                    self.include_regexps.append(re.compile(pattern))
            except Exception as exception:
                self.warn("cannot parse regexp \"%s\" so ignoring it: %s" % (parsing_pattern, str(exception)))

    # -----------------------------------------------------------------
    def should_include(self, name):
        
        # Keep all if nothing was ever configured.
        if self.include_regexps is None:
            keep = True
        else:            
            keep = False
            for regexp in self.include_regexps:
                if regexp.match(name):
                    keep = True
                    break
        return keep

    # -----------------------------------------------------------------
    def warn(self, message):
        logging.warning(message)

    # -----------------------------------------------------------------
    def debug(self, message):
        logging.debug(message)
