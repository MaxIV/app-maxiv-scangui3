#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="taurusgui-scangui3",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="GUI for scanning sardana motors",
    author="Ludvig Kjellsson",
    author_email="ludvig.kjellsson@maxlab.lu.se",
    license="GPL-3.0-or-later",
    url="https://gitlab.maxiv.lu.se/kits-maxiv/app-maxiv-scangui",
    package_dir={"": "src"},
    packages=find_packages("src"),
    include_package_data=True,
    package_data={
        "scangui": [
            "default.ini",
            "images/MAXlogo.jpg",
            "images/motorscanguisplash.jpg",
        ]
    },
    data_files=[("share/applications", ["maxiv-scangui.desktop"])],
    install_requires=["h5py>=3.5.0", "numpy", "pyqtgraph", "watchdog", "sardana"],
    entry_points={
        "console_scripts": ["scangui = scangui.scangui:main"]
    },
)
