# SCANGUI

Scangui is a GUI for scanning sardana motors.

## INFORMATION

To use the scangui you need the following

* Access to sardana door
* Configured folder where client can access .h5 data saved by macroserver

## INSTALLATION

Install the package

    pip install git+https://gitlab.com/MaxIV/app-maxiv-scangui3

Launch the gui by using the launcher in the menu or by running

    scangui

If not configured the gui asks for input:

* Lists of motors to be scanned. If left empty the GUI will dynamically get all motors from sardana.
* Path to where the sardana .h5 files are accessible for the client
* Macroserver/Door

If the sardana jsonrecorder is not enabled the GUI will ask you if it should enable it for you.

The config files are stored in  ~/.config/MAXIV/scangui.ini

## COMMAND LINE

option: ```--movable_filters_filename={filename}```  
default: none (all movables will be included)  
example: ```--movable_filters_filename=movables.json```  

The filename must exist in the folder ```~/.config/MAXIV```.
If the file does not exist or has invalid json contents, Scangui will print an error to the console.
Otherwise, it will be loaded and used as a filter when making the list of Sardana motors to be included in the dropdowns.
The items in the list are regular expressions.
Any list item with an invalid regex expression is ignored.

Here is a valid example file:

    {
        "include": [
            "^.*[0][1]$",
            "mot03"
        ]
    }

In this example, all Sardana motors ending in "01", as well as motor named "mot03" are included.  All others are excluded.

## GUI MAIN FEATURES
The `scangui` and its main features are illustrated in **Figure 1**. Where:
- **a**: drop-down list with all current supported macros (e.g., `ascan`, `umv`, `relmaclib`, ...)
- **b**: the parameters of a macro can be configured individually, followed by its name and type, if the selected macro in **a** supports any parameters
- **c**: besides selecting and configuring the macro with **a** and **b**, it is also possible to just type the macro command in the available command line. Also, a progress bar of the current scan is available.
- **d**: the experiment config widget (`expconf`) is embedded in the application
- **e**: a data liveviewer is available in the *scanplotter* tab where the X and Y axes can be configured to plot the data of any experimental channel in the measurement group and/or a motor used in the current scan. Only exp. channels with data being recorded through the MacroServer are supported on this case.


<table align="center"><tr><td align="center" width="9999">
<img src="doc/scangui3.png" align="center" width="" alt="Project icon">

---

Figure 1: ScanGUI3 main features
</td></tr></table>

<br></br>

A quick demonstration of how to use the main features provided by ScanGUI is available in the video below.

<table align="center"><tr><td align="center" width="9999">
<img src="doc/scangui3.mov" align="center" width="" alt="scangui">

---

Video 1: scangui demo with `ascan`
</td></tr></table>


## FAQ

* How to change list of motors?
  Right click on the basic executor and select "Edit motor list..."

## DEVELOPER INFORMATION

The app is split into two taurus panels, one for starting execution and one for plotting and handling the files. Also added to the interface is the taurus macroexecutor and the macrosequencer.

The scangui subscribed to the recorddata events from the macroserver door.

A watchdog is frequently looking at directory where sardana stores it files, once it detects an update the gui will load the new plots from the .h5 file.

It will plot data from the files and from the macrodoor separately.

The gui is created with PyQt and the plots are produced with pyqtgraph.

There is an exercise script for the movable filtering which can be run by:

    pip install -e .
    python exercises/movable_configuration.py

Successful output looks like:

    .......... keep "any"? True
    .......... keep "any"? True
    WARNING:root:cannot parse regexp "ab[c" so ignoring it: unexpected end of regular expression
    .......... keep "any"? False
    .......... keep "mot01"? True
    .......... keep "mot02"? False
    .......... keep "mot03"? True
    .......... keep "gap01"? True
    .......... keep "gap02"? False

### DEVELOPING LOCALLY

You need a running instance of Sardana.

We recommend using conda to install all dependecencies from conda-forge:

```bash
conda create -y -n scangui python=3.11 sardana pyqtgraph watchdog h5py
conda activate scangui
pip install -e .
```
