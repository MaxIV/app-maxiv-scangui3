widgets Package
===============

:mod:`mathsettings` Module
--------------------------

.. automodule:: motorscangui.motorscanplotter.scanitems.widgets.mathsettings
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`roisettings` Module
-------------------------

.. automodule:: motorscangui.motorscanplotter.scanitems.widgets.roisettings
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`settings` Module
----------------------

.. automodule:: motorscangui.motorscanplotter.scanitems.widgets.settings
    :members:
    :undoc-members:
    :show-inheritance:

