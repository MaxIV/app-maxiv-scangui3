Welcome to Motorscangui's documentation!
========================================
Species motorscangui is a tool to easily scan sardana with different macros without having to use the command line.

This is the developer documentation. The user manual can be found at http://wiki.maxiv.lu.se/index.php/SPECIES_MOTORSCANGUI

The app is split into two taurus panels, one for starting execution and one for plotting and handling the files. Also added to the interface is the taurus macroexecutor and the macrosequencer.

The app uses a taurus door to start the macro and get events in JSON format from the macroserver.

A watchdog is frequently polling a directory where sardana stores it files, once it detects an update the gui will load the new plots from the .h5 file.

The gui is created with PyQt and the plots a produced with pyqtgraph.

Contents:

.. toctree::
   :maxdepth: 3

   motorscangui <motorscangui>

      motorscanplotter <motorscangui.motorscanplotter>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

