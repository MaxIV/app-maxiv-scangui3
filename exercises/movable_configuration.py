import sys
import traceback
import time
import json
from scangui.movables import MovableConfiguration


class Test(object):
    # -------------------------------------------------------------------------
    def run(self):
        '''
        Run the test.
        '''

        try:
            doing = "instantiating object"

            targets = ["any"]

            # Instantuate the configurator.
            movable_configuration = MovableConfiguration()
                    
            # Test null argument.
            jsondict = None
            movable_configuration.configure_from_jsondict(jsondict)
            for target in targets:
                print(".......... keep \"%s\"? %s" % (target, movable_configuration.should_include(target)))
                    
            # Test empty argument.
            jsondict = {}
            movable_configuration.configure_from_jsondict(jsondict)
            for target in targets:
                print(".......... keep \"%s\"? %s" % (target, movable_configuration.should_include(target)))
                    
            # Test bad regexp.
            jsondict = {"include": ["ab[c"]}
            movable_configuration.configure_from_jsondict(jsondict)
            for target in targets:
                print(".......... keep \"%s\"? %s" % (target, movable_configuration.should_include(target)))
                    
            # Test an actual filter.
            jsondict = {"include": ["^.*[0][1]$", "mot03"]}
            movable_configuration.configure_from_jsondict(jsondict)

            targets = ["mot01", "mot02", "mot03", "gap01", "gap02"]

            for target in targets:
                print(".......... keep \"%s\"? %s" % (target, movable_configuration.should_include(target)))

        except Exception as exception:
                print("========== unexpected exception while %s: %s" % (doing, exception))
                traceback.print_exc(file=sys.stdout)

# -------------------------------------------------------------------------
'''
Run the test class.
'''
test = Test()
test.run()
